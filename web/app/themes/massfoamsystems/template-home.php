<?php
/**
 * Template Name: Home Page
 */

while (have_posts()) : the_post();
  get_template_part('templates/blocks/home', 'selection');
  // get_template_part('templates/blocks/home', 'hero-slider');
  get_template_part('templates/blocks/home', 'intro-section');
  get_template_part('templates/blocks/global', 'why-use-mass-foam');
  get_template_part('templates/blocks/home', 'about-us');
  get_template_part('templates/blocks/global', 'review-feed');
  get_template_part('templates/blocks/home', 'our-sectors');
  get_template_part('templates/blocks/home', 'benefits-of-icynene');
  get_template_part('templates/blocks/global', 'worked-with');
  get_template_part('templates/blocks/home', 'case-studies');
  get_template_part('templates/blocks/global', 'accredited-trustworthy');
  get_template_part('templates/blocks/home', 'featured-blogs');
  get_template_part('templates/blocks/global', 'footer-contact-cta');
endwhile;