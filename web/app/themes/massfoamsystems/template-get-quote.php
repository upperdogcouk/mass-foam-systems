<?php
/**
 * Template Name: Get a Quote
 */

while (have_posts()) : the_post();
  get_template_part('templates/blocks/global', 'page-hero');
  get_template_part('templates/content', 'get-quote');
  get_template_part('templates/blocks/global', 'review-feed');

endwhile;