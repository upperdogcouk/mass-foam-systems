<?php
/**
 * Template Name: About
 */

while (have_posts()) : the_post();
  get_template_part('templates/blocks/global', 'page-hero');
  get_template_part('templates/content', 'page');
  get_template_part('templates/blocks/about', 'grid');
  get_template_part('templates/blocks/global', 'accredited-trustworthy');
  get_template_part('templates/blocks/global', 'review-feed');
endwhile;