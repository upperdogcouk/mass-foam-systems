<?php
/**
 * Template Name: Eco Calculator
 */

while (have_posts()) : the_post();
  get_template_part('templates/blocks/global', 'page-hero');
  get_template_part('templates/content', 'eco-eligibility');
  get_template_part('templates/blocks/global', 'review-feed');

endwhile;