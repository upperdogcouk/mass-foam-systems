<?php get_template_part('templates/blocks/global', 'page-hero'); ?>

<div class="container">
    <div class="page-content-wrap">
        <div class="page-content">
            <?php get_template_part('templates/page', 'title'); ?>

            <div class="gform_wrapper faq-search-form mb-1_75">
                <form action="<?php echo home_url('/'); ?>" class="row no-gutters text-center text-md-left align-items-center">
                    <input class="col px-0_5 mb-0" name="s" type="search" placeholder="Search...">
                    <input type="hidden" name="post_type" value="faq">
                    <button type="submit" class="mf-button col-auto px-0_75">Search FAQs</button>
                </form>
            </div>

            <div class="row">
                <div class="col-md">

                    <?php
                        $faq_cats = get_terms( array(
                            'taxonomy' => 'faq-categories',
                            'hide_empty' => false,
                        ));

                        foreach ($faq_cats as $f_cat): // Loop through FAQ categories
                            $cat_ID = $f_cat->term_id;

                            $args = array(
                                'number_posts' => -1,
                                'post_type' => 'faq',
                                'post_status' => 'publish',
                                'meta_key' => 'featured_faq',
                                'orderby' => 'meta_value',
                                'order'	=> 'DESC',
                                'tax_query' => array(
                                    array(
                                      'taxonomy' => 'faq-categories',
                                      'field' => 'term_id',
                                      'terms' => $cat_ID,
                                    )
                                )
                            );

                        $faq_posts = get_posts( $args );
                        if($faq_posts):

                    ?>

                        <h3><?php echo $f_cat->name ?></h3>
                        <hr class="hr-70 ml-0">

                        <div class="row mb-1_5">
                            <?php foreach($faq_posts as $f_post): // Loop through questions within FAQ Category
                                $featured_faq = get_field('featured_faq', $f_post->ID);

                            if($featured_faq) { ?>
                                <div class="col-sm-6 mb-0_5">
                                    <a class="faq-question-card featured-question d-flex flex-column" href="<?php echo get_permalink($f_post->ID);?>">
                                        <h3 class="body-large-size mb-0_5"><?php echo $f_post->post_title;?></h3>
                                        <hr class="hr-wild-sand">
                                        <span class="mf-text-button align-self-start">Read More</span>
                                    </a>
                                </div>

                            <?php } else { ?>

                                <div class="col-12 mb-0_5">
                                    <a href="<?php echo get_permalink($f_post->ID);?>" class="faq-question-card d-block d-sm-flex align-items-start justify-content-between">
                                        <p class="mb-0 pr-sm-1"><?php echo $f_post->post_title;?></p>
                                        <span class="mf-text-button">Read More</span>
                                    </a>
                                </div>

                            <?php } endforeach; // End Question Loop ?>

                        </div>

                    <?php endif; endforeach; // End FAQ Category Loop ?>

                </div>

                <?php get_template_part('templates/blocks/faq', 'sidebar'); ?>

            </div>

        </div>
    </div>
</div>

<?php get_template_part('templates/blocks/global', 'review-feed'); ?>