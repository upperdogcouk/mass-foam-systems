<?php
global $wp_query;
$total_results = $wp_query->found_posts;
$name = $wp_query->query['s'];
$results = $wp_query->post_count;

get_template_part('templates/blocks/global', 'page-hero'); ?>

<div class="container">
    <div class="page-content-wrap">
        <div class="page-content">
            <div class="row">
                <div class="col-md">
                    <h3 class="mb-1_5">Your search for '<?php echo $name; ?>' returned <?php echo $results; ?> FAQ result<?php if ($results != 1 ) echo 's' ;?></h3>

                    <?php if (!have_posts()) : ?>
                        <p><?php _e('Sorry, no results were found.', 'sage'); ?></p>
                    <?php endif; ?>

                    <?php while (have_posts()) : the_post(); ?>
                        <?php get_template_part('templates/content', 'search'); ?>
                    <?php endwhile; ?>
                </div>

              <?php get_template_part('templates/blocks/faq', 'sidebar'); ?>
            </div>

            <?php the_posts_navigation(); ?>

        </div>
    </div>
</div>

<?php get_template_part('templates/blocks/global', 'review-feed'); ?>