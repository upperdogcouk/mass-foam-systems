<?php get_template_part('templates/blocks/global', 'page-hero'); ?>

<div class="container">
    <div class="page-content-wrap blog-listing">
        <div class="page-content">
            <?php get_template_part('templates/page', 'title'); ?>
            <?php get_template_part('templates/blocks/blog', 'category-buttons'); ?>

            <div class="blog-posts-list row">
                <?php
                    $count = 0;
                    while (have_posts()) : the_post();
                    $count++;
                    $featured = (($count < 3 && !is_paged()) ? true : false);
                    $column_classes = $featured ? 'col-sm-6 mb-1' : 'col-sm-6 col-md-4 mb-1';
                    $category = get_the_category();
                ?>
                    <div class="<?php echo $column_classes;?>">
                        <div class="blog-card">
                            <div class="blog-image">
                                <a href="<?php the_permalink();?>">
                                    <img class="img-fluid w-100 lozad" src="<?php asset_uri('images/blog-no-image.png');?>" data-src="<?php echo (get_the_post_thumbnail() ? get_the_post_thumbnail_url($post, 'blog_430') : get_asset_uri('images/blog-no-image.png'));?>" alt="mass foam system logo">
                                </a>
                                <span class="category-badge cat-<?php echo $category[0]->slug;?>"><?php echo $category[0]->name;?></span>
                            </div>
                            <div class="p-0_75">
                                <h3 class="mb-0"><?php the_title();?></h3>
                                <p class="byline author vcard body-smallest-size mb-0_5">by <?php echo get_the_author();?></p>

                                <?php if($featured) echo '<p class="mb-0">' . mb_strimwidth(strip_tags(get_the_content()), 0, 160, '...') . '</p>'; ?>
                            </div>

                            <div class="row no-gutters link-date justify-content-between">
                                <div class="col-auto py-0_5 px-0_75">
                                    <a href="<?php the_permalink();?>" class="mf-text-button">Read More</a>
                                </div>
                                <div class="col-auto py-0_5 px-0_75">
                                    <time datetime="<?php echo get_post_time('c', true);?>" class="colour-boulder m-0 body-small-size updated">
                                        <?php echo get_post_time('l j F Y');?>
                                    </time>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>

          <?php the_posts_navigation(); ?>

        </div>
    </div>
</div>

<?php get_template_part('templates/blocks/global', 'review-feed'); ?>
