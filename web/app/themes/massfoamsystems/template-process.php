<?php
/**
 * Template Name: Our Process
 */

while (have_posts()) : the_post();
  get_template_part('templates/blocks/global', 'page-hero');
  get_template_part('templates/content', 'process');
  get_template_part('templates/blocks/global', 'review-feed');

endwhile;