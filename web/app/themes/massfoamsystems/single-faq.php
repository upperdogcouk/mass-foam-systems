<?php
  get_template_part('templates/blocks/global', 'page-hero');
  get_template_part('templates/content-single', get_post_type());
  get_template_part('templates/blocks/global', 'footer-contact-cta');
?>
