<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php',// Theme customizer
  'lib/acf.php',       // ACF Stuff
  'lib/post_types.php',// Custom Post Types
  'lib/quote-calculator.php', // Quote calculator library
  'lib/eligibility-calculator.php', // Grant Scheme Checker
  'lib/eco-calculator.php' // Eco Calculator
];
foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);


// Asset call function
function asset_uri($asset) {
  echo Roots\Sage\Assets\asset_path($asset);
}

function get_asset_uri($asset) {
  return Roots\Sage\Assets\asset_path($asset);
}

// changing the logo link from wordpress.org to your site
function custom_login_url() {
  return home_url();
}

add_filter('login_headerurl', 'custom_login_url');

// changing the alt text on the logo to show your site name
function custom_login_title() {
  return get_option('blogname');
}

add_filter('login_headertext', 'custom_login_title');

// Move Yoast to bottom
function yoasttobottom() {
  return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');


/**
 * Add image to Spray Foam Services Menu
 */

add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);

function my_wp_nav_menu_objects( $items, $args ) {

  // loop
  foreach( $items as $item ) {

    // vars
    $para = get_field('spray_foam_menu_text', $item);
    $img = get_field('spray_foam_menu_image', $item);

    // add paragraph & text.
    if( $img ) {
      $item->title = '<span class="non-link">' . wp_get_attachment_image($img['ID'], 'medium') . '<span class="menu-big-title">' . $item->title . '</span>';
    }

    if( $para ) {
      $item->title .= '<span class="menu-paragraph">' . $para . '</span></span>';
    }
  }

  // return
  return $items;

}


// Add Shortcode
function get_quote_shortcode( $atts ) {

  // Attributes
  $atts = shortcode_atts(
    array(
      'text' => '',
    ),
    $atts
  );

  return '<div class="get_quote_cta">
            <p>' . $atts['text'] . '</p>
            <a class="mf-button white-button" href="' . get_permalink( get_page_by_path( 'request-a-call-back' ) ) . '">Request a call back</a>
          </div>';


}
add_shortcode( 'get_quote_cta', 'get_quote_shortcode' );

function get_accreditations_text_list( $atts ) {
  $accreditations = get_field('accreditations', 'option');
  $text = '<ul>';
  foreach ($accreditations as $accreditation) {
    $text .= '<li>' . $accreditation['accreditation_name'] . '</li>';
  }
  return $text . '</ul>';
}
add_shortcode( 'get_accreditations_text_list', 'get_accreditations_text_list' );

function get_quote_options($atts) {

  // Attributes
  $atts = shortcode_atts(
    array(
      'text' => '',
      'option_1_text' => '',
      'option_1_link' => '',
      'option_2_text' => '',
      'option_2_link' => '',
    ),
    $atts
  );

  return '<div class="get_quote_options">
          <p>' . $atts['text'] . '</p>
          <a class="mf-button white-button" href="' . $atts['option_1_link'] . '">' . $atts['option_1_text'] . '</a>
          <a class="mf-button white-button" href="' . $atts['option_2_link'] . '">' . $atts['option_2_text'] . '</a>
          </div>';
}
add_shortcode('get_quote_options', 'get_quote_options');

function get_icynene_reviews() {
  ob_start();
  get_template_part('templates/blocks/global', 'icynene-reviews');
  return ob_get_clean();
}
add_shortcode('get_icynene_reviews', 'get_icynene_reviews');

function get_the_team() {
  ob_start();
  get_template_part('templates/blocks/global', 'the-team');
  return ob_get_clean();
}
add_shortcode('get_the_team', 'get_the_team');

function contact_bar() {
  return '<div class="contact_bar">
            <span>Get a no obligation quote today!</span>
            <a class="mf-button white-button" href="tel:08002465051">0800 246 5051</a>
          </div>';
}

add_shortcode('contact_bar', 'contact_bar');

function trustpilot_slider($atts)
{
  // Attributes
  $atts = shortcode_atts(
    array(
      'stars' => '4.5,5',
      'tag' => '',
    ),
    $atts
  );

  return '<div class="trustpilot-widget" data-locale="en-GB" data-template-id="54ad5defc6454f065c28af8b" data-businessunit-id="5ad8867d40ff7f0001ef011b" data-style-height="240" data-style-width="100%" data-theme="light" data-stars="' . $atts['stars'] . '" ' . ($atts['tag'] ? 'data-tags="' . $atts['tag'] . '" ' : '') . 'data-review-languages="en" data-font-family="Arimo">
            <a href="https://uk.trustpilot.com/review/www.massfoamsystems.co.uk" target="_blank" rel="noopener">Trustpilot</a>
          </div>';
}

add_shortcode('trustpilot_slider', 'trustpilot_slider');

/**
 * Count views of post, to allow 'Popular FAQs'
 */

function wpb_set_post_views($postID) {
  $count_key = 'wpb_post_views_count';
  $count = get_post_meta($postID, $count_key, true);

  if($count==''){
    $count = 0;
    delete_post_meta($postID, $count_key);
    add_post_meta($postID, $count_key, '0');
  }else{
    $count++;
    update_post_meta($postID, $count_key, $count);
  }
}

//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10);


function wpb_track_post_views ($post_id) {
  if ( !is_single() ) return;
  if ( empty ( $post_id) ) {
    global $post;
    $post_id = $post->ID;
  }
  wpb_set_post_views($post_id);
}
add_action( 'wp_head', 'wpb_track_post_views');
