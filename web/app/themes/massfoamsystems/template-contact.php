<?php
/**
 * Template Name: Contact
 */

while (have_posts()) : the_post();
  get_template_part('templates/blocks/global', 'page-hero');
  get_template_part('templates/content', 'page');
  get_template_part('templates/blocks/contact', 'grid');
  get_template_part('templates/blocks/global', 'accredited-trustworthy');
  get_template_part('templates/blocks/global', 'review-feed');
endwhile;