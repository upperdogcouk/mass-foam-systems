export default {
  init() {
    // JavaScript to be fired on the single post page
  },
  finalize() {
    // JavaScript to be fired on the single post page, after the init JS

      if ( $('#blogGallery .blog_gallery_slide').length > 1 ) {


          $('#blogGallery').slick({
              slidesToShow: 1,
              slidesToScroll: 1,
              dots: false,
              arrows: true,
              infinite: true,
              speed: 400,
              rows: 0,
              autoplay: true,
              autoplaySpeed: 6000,
          });
      }

      // If there is a FAQ section
      if ($('.question-container').length) {
        var $transition_duration = 300;
        $('.question-container .question .show-more').css({transition: `all ${$transition_duration}ms`});
        $('.question-container ul').on('click', 'li .question', (elem) => {   // On the click of a list element
          var $question_elem = $(elem.currentTarget).next('.question-text');    // Store the question short answer
          if ($question_elem.is(':visible')) {    // If the element is already visible
            $question_elem.fadeOut($transition_duration);   // Hide the element
            $(elem.currentTarget).find('.show-more').css({transform: 'rotate(0deg)'});    // Rotate the arrow back to normal
          }
          else {    // If the element is not visible
            $('.question-container .question-text').fadeOut($transition_duration);    // Hide all other elements
            $(elem.currentTarget).find('.show-more').css({transform: 'rotate(90deg)'});   // Rotate the arrow to point down
            $question_elem.fadeIn($transition_duration);   // Show the element
          }
        });
      }
  },
};
