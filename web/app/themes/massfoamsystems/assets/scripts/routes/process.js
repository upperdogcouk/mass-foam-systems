export default {
    init() {
        // JavaScript to be fired on the home page
    },
    finalize() {
        // JavaScript to be fired on the Why Icynene page, after the init JS
        $(".step-trigger").click(function (e) {
            e.preventDefault();

            var target = $(this).data("target");

            $('html, body').animate({
                scrollTop: $("." + target).offset().top - 100,
            }, 500);
        });
    },
};
