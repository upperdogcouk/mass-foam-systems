/*eslint no-undef: "warn"*/
/* eslint-disable */

import "slick-carousel/slick/slick.js";
import "ekko-lightbox/dist/ekko-lightbox.min";
import lozad from "lozad/dist/lozad.js";

export default {
  init() {
    // JavaScript to be fired on all pages
    const observer = lozad(); // lazy loads elements with default selector as '.lozad'
    observer.observe();
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired

    // Header change on scroll
    $(function () {
      //caches a jQuery object containing the header element
      var header = $(".main-header");
      var footerCTA = $(".mobile-footer-cta");
      $(window).scroll(function () {
        var scroll = $(window).scrollTop();

        if ($(window).width() > 860.02) {
          if (scroll > 80) {
            header.addClass("scroll-shrink");
          } else if (scroll < 40) {
            header.removeClass("scroll-shrink");
          }
        }

        if (scroll >= 200) {
          footerCTA.addClass("slide-in");
        } else {
          footerCTA.removeClass("slide-in");
        }
      });
    });

    // Mobile Navigation
    $(".mobile-nav .big-dropdown > a").click(function () {
      $(this).parent("li").toggleClass("show-sub-menu");
    });

    $("#nav-toggle-open").click(function () {
      $(".mobile-nav-wrapper").addClass("active");
      $(".main-header").addClass("mob-nav-open");
    });

    $("#nav-toggle-close").click(function () {
      $(".mobile-nav-wrapper").removeClass("active");
      $(".main-header").removeClass("mob-nav-open");
    });

    // Lightbox
    $(document).on("click", '[data-toggle="lightbox"]', function (event) {
      event.preventDefault();
      $(this).ekkoLightbox();
    });

    // Trustpilot Review Slider
    $(".review-slider").slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      dots: false,
      arrows: false,
      infinite: true,
      speed: 400,
      rows: 0,
      autoplay: true,
      autoplaySpeed: 6000,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
          },
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    });

    /**
     * Read Load More
     */

    if ($(".expanded-content").length) {
      var fullHeight = $(".expanded-content-inner").height();
      $(".expanded-content-inner").addClass("height-restrict");

      $("body").on("click", ".js-expand-read-more", function () {
        // Close Expansion and reset
        if ($(".expanded-content-inner").hasClass("expanded")) {
          $(".expanded-content-inner")
            .css("max-height", "0px")
            .removeClass("expanded");
          $(".js-expand-read-more").text("Read More");
          $("html, body").animate(
            {
              scrollTop: $(".expanded-content").offset().top - 125,
            },
            500
          );
        } else {
          $(".expanded-content-inner")
            .css("max-height", fullHeight + 45)
            .addClass("expanded");
          $(".js-expand-read-more").text("Read Less");
        }
      });
    }

    // Accreditations scroller
    $(".accreditation-logos .row").slick({
      slidesToScroll: 1,
      swipeToSlide: true,
      infinite: true,
      autoplay: true,
      autoplaySpeed: 2000,
      arrows: false,
      easing: "ease",
      lazyLoad: "progressive",
      variableWidth: true,
    });

    // Meet the team scroller
    $(".the-team .row").slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      swipeToSlide: true,
      infinite: true,
      autoplay: true,
      autoplaySpeed: 2000,
      arrows: false,
      easing: "ease",
      lazyLoad: "progressive",
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    });

    // Worked with slider
    $(".worked-with-logos .row").slick({
      slidesToScroll: 1,
      swipeToSlide: true,
      infinite: true,
      autoplay: true,
      autoplaySpeed: 2000,
      arrows: false,
      easing: "ease",
      lazyLoad: "progressive",
      variableWidth: true,
    });

    /**
     * Google Analytics Events
     */

    // Click to call event tracking
    $("a[href^='tel:']").click(function () {
      gtag("event", "Click to Call", {
        event_category: "Phone Call Tracking",
        event_label: "",
        value: 0,
      });

      fbq("track", "Contact");
    });

    // Trustpilot Review Click Tracking
    $(".trustpilot-widget img").click(function () {
      gtag("event", "Click", {
        event_category: "Trustpilot Review",
        event_label: "",
        value: 0,
      });
    });

    $(".trustpilot-widget").click(function () {
      gtag("event", "Click", {
        event_category: "Trustpilot Widget",
        event_label: "",
        value: 0,
      });
    })

    // Request call back submit click tracking
    $("#gform_submit_button_3").click(function () {
      gtag("event", "Submit", {
        event_category: "Request a Callback",
        event_label: "",
        value: 0,
      });

      fbq("track", "Contact");
    });

    // Finance option click tracking
    $(".finance-widget").click(function () {
      gtag("event", "Submit", {
        event_category: "Finance Option",
        event_label: "",
        value: 0,
      });

      fbq("track", "Lead");
    });

    // Contact form Tracking
    $(document).bind("gform_confirmation_loaded", function () {
      gtag("event", "Submit", {
        event_category: "Contact Form",
        event_label: "",
        value: 0,
      });

      fbq("track", "Lead");
    });
  },
};
