export default {
  init() {
    // JavaScript to be fired on the home page
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS

    // Make the video play once slick has loaded
    $(".home-hero-slider").on("init", function () {
      $("video")[0].play();
    });

    if ($(".home-hero-slider .slide").length > 1) {
      $(".home-hero-slider").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        infinite: true,
        speed: 400,
        rows: 0,
        autoplay: true,
        autoplaySpeed: 6000,
        appendDots: ".dots-holder",
      });
    }

    $(".video-modal").on("shown.bs.modal", function (e) {
      var target = $(e.relatedTarget);
      var vid_id = target.attr("data-vid");
      var vid_src =
        "https://www.youtube.com/embed/" +
        vid_id +
        "?rel=0&showinfo=0&autoplay=1";

      $(this).find("iframe").attr("src", vid_src);
    });

    $(".video-modal").on("hidden.bs.modal", function () {
      $(this).find("iframe").attr("src", null);
    });
  },
};
