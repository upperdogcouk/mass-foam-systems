// Import quote calculator lib
import EcoCalculator from "../util/EcoCalculator.js";

export default {
    init() {
        // JavaScript to be fired on the home page
    },
    finalize() {
        // JavaScript to be fired on the home page, after the init JS

        // instantiate instance of eligibility calculator
        new EcoCalculator();

    },
};
