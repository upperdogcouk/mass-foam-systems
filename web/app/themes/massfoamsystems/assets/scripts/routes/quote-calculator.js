// Import quote calculator lib
import QuoteCalculator from "../util/QuoteCalculator.js";

export default {
    init() {
        // JavaScript to be fired on the home page
    },
    finalize() {
        // JavaScript to be fired on the home page, after the init JS

        // instantiate instance of quote calculator
        new QuoteCalculator();

    },
};
