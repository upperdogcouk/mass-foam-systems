export default {
    init() {
        // JavaScript to be fired on the home page
    },
    finalize() {
        // JavaScript to be fired on the Why Icynene page, after the init JS
        $("#nav-tab li a").click(function () {
            var that = this;
            setTimeout(function () {
                if ($(that).hasClass("active")) {
                    var index = $("#nav-tab li").index($(that).parent());
                    var transX = index * 100 + '%';
                    $('#nav-tab .magic-box').addClass("active").css({'transform': 'translateX(' + transX + ')'});
                }
            }, 100);
        });
    },
};
