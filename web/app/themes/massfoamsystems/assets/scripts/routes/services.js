export default {
  finalize() {
    // JavaScript to be fired on the services page
      $('.services-gallery-slider').slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: false,
          arrows: true,
          infinite: true,
          speed: 400,
          rows: 0,
          autoplay: true,
          autoplaySpeed: 3000,
      });
  },
};
