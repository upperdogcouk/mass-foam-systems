/*eslint no-undef: "warn"*/

"use strict";

/**
 * EcoCalculator class
 */
class EcoCalculator {
  /**
   * Execute when instance created
   */
  constructor() {
    // set ajax url
    this.ajaxurl = window.ajaxurl;

    // start listening for certain events
    this.listeners();
  }

  /**
   * Listens for user input and events
   */
  listeners() {
    // set that var to enable child functions to scope outside
    let that = this;

    // submit button listener (next step)
    $(document.body).on(
      "click",
      ".eco-submit, input[type=radio]",
      function (e) {
        // prevent default action if not a radio
        if ($(this).attr("type") !== "radio") {
          e.preventDefault();
        }

        // clear old error messages
        that.clear_errors();


        // submit step
        that.submit_step(this);
      }
    );
  }

  /**
   * Switches Step
   */
  show_step(oldstep, newstep) {
    // remove active & show class's
    $("#eco-step-" + oldstep)
      .removeClass("active")
      .addClass("show");
    $(".calc-step.active").removeClass("active");

    // show next step
    $("#eco-step-" + newstep)
      .show()
      .addClass("active");

    // when user goes back to previous step hide future steps
    var dontshow = false;
    $(".mfs-form .calc-step").each(function () {
      if ($(this).hasClass("active")) dontshow = true;

      if (dontshow) {
        $(this).removeClass("show").attr("style", "");
      }
    });

    // scroll to next step
    setTimeout(function () {
      $("html,body").animate(
        { scrollTop: $("#eco-step-" + newstep).offset().top - 400 },
        500
      );
    }, 100);

    // send on first step complete
    if (newstep === 2) {
      gtag("event", "Click", {
        event_category: "ECO4 Form First Step",
        event_label: "",
        value: 0,
      });

      fbq("trackCustom", "ECO4 Form 1st Step");
    }

    // send on form complete
    if (newstep === 7) {
      gtag("event", "Click", {
        event_category: "ECO4 Form Completed",
        event_label: "",
        value: 0,
      });

      fbq("trackCustom", "ECO4 Form Submit");
    }
  }

  /**
   * Clear errors
   */
  clear_errors() {
    $("input.error").removeClass("error");
    $(".field-err-msg").fadeOut(300, () => {
      $(this).remove();
    });
  }

  /**
   * Show errors on fields
   */
  show_errors(errors) {
    if (errors.fields) {
      $.each(errors.fields, (index, value) => {
        let err_field = $(`input[name="${index}"]`);

        $(err_field).addClass("error");
        $(
          `<span class='field-err-msg field-err-msg-${index}'>${value}</span>`
        ).insertAfter(err_field);
      });
    }

    // if session expires boot user to start
    if (errors.session_expired) {
      alert("Your session has expired, please re-enter your details again.");
      this.reset_form();
    }
  }

  /**
   * Toggle loading class
   */
  toggle_loading_class(show) {
    if (show) {
      $("body").addClass("loading");
    } else {
      $("body").removeClass("loading");
    }
  }

  /**
   * Show and hide elements
   */
  show_hide_elements(show_elem, hide_elem) {
    if (show_elem) $(show_elem).show();

    if (hide_elem) $(hide_elem).hide();
  }

  /**
   * Shows a message hiding every other stage
   */
  show_message(response) {
    // get target element
    var target_elem = "#" + response.success.message_id;

    // hide all other step screens
    $(".calc-step").removeClass("active show");

    // show only the target element
    $(target_elem).addClass("show active");

    // scroll to modal
    $("html,body").animate(
      { scrollTop: $(target_elem).offset().top - 200 },
      500
    );
  }

  /**
   * Executed when user clicks to go to next step
   */
  submit_step(submit_button) {
    // set that var to enable child functions to scope outside
    var that = this;

    // find form in step
    var form = $(submit_button).closest("form");

    // collect field data and convert to JSON array
    var formdata = $(form).serializeArray();

    // get current step
    var step = $(form).find('input[name="step"]').val();

    // add loading class to body
    that.toggle_loading_class(true);

    // post data to ajax processor url
    $.post(
      that.ajaxurl + "?action=ecocalc",
      formdata,
      function (response) {
        // convert json to array
        response = JSON.parse(response);

        // if we get a successful response
        if (response.success) {
          // ** SUCCESS **

          // show new step and jump to it
          if (response.success.go_to_step) {
            that.show_step(step, response.success.go_to_step);
          }

          // if there are any elements to show and hide
          if (response.success.show_elem || response.success.hide_elem)
            that.show_hide_elements(
              response.success.show_elem,
              response.success.hide_elem
            );
        } else if (response.errors) {
          // ** ERROR **

          // display errors
          that.show_errors(response.errors);
        }

        // remove loading class on body
        that.toggle_loading_class(false);
      }
    );
  }
}

export default EcoCalculator;
