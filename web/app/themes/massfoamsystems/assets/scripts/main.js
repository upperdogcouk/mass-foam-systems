// import external dependencies
import 'jquery';

// Import everything from autoload
import "./autoload/**/*";

// import local dependencies
import common from './routes/common';
import pageTemplateTemplateEcoCalc from "./routes/eco-calculator";
import pageTemplateTemplateGrantScheme from './routes/eligibility-calculator';
import home from './routes/home';
import pageTemplateTemplateProcess from './routes/process';
import pageTemplateTemplateGetQuote from './routes/quote-calculator';
import pageTemplateTemplateServices from './routes/services';
import single from './routes/single';
import pageTemplateTemplateWhyicynene from './routes/whyicynene';
import Router from './util/Router';

/** Populate Router instance with DOM routes */
const routes = new Router({
    // All pages
    common,
    // Home page
    home,
    // Services page
    pageTemplateTemplateServices,
    // Single blog page
    single,
    // Why page
    pageTemplateTemplateWhyicynene,
    // Process
    pageTemplateTemplateProcess,
    // Quote calculator
    pageTemplateTemplateGetQuote,
    // Eligibility calculator
    pageTemplateTemplateGrantScheme,
    // Eco Calculator
    pageTemplateTemplateEcoCalc,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
