<?php
/**
 * Template Name: Grant Scheme
 */

while (have_posts()) : the_post();
  get_template_part('templates/blocks/global', 'page-hero');
  get_template_part('templates/content', 'check-eligibility');
  get_template_part('templates/blocks/global', 'review-feed');

endwhile;