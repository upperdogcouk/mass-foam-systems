<?php
/**
 * Template Name: Why Icynene
 */

while (have_posts()) : the_post();
    get_template_part('templates/blocks/global', 'page-hero');
    get_template_part('templates/content', 'whyicynene');
    get_template_part('templates/blocks/global', 'accredited-trustworthy');
    get_template_part('templates/blocks/global', 'review-feed');
    get_template_part('templates/blocks/global', 'footer-contact-cta');
endwhile;