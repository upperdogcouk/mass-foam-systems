<?php get_template_part('templates/blocks/global', 'page-hero'); ?>

<div class="container">
    <div class="page-content-wrap">
        <div class="page-content">
            <?php get_template_part('templates/page', 'title'); ?>
            <p><?php _e('Sorry, but the page you were trying to view does not exist.', 'sage'); ?></p>
            <p><?php _e('Please try using the menu above to find what you\'re looking for, or give us a call - we\'ll be happy to help!', 'sage'); ?></p>
        </div>
    </div>
</div>






