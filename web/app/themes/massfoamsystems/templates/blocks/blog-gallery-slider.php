<?php
    $slides = get_field('blog_gallery_slides');
    if($slides):
?>
    <div class="blog_gallery" id="blogGallery">
        <?php foreach($slides as $slide_i => $slide_k):?>
            <div class="blog_gallery_slide">
                <?php echo wp_get_attachment_image($slide_k['ID'], 'large', '', array('class' => 'img-fluid'));?>
                <?php if($slide_k['caption']):?>
                    <figcaption><?php echo $slide_k['caption'];?></figcaption>
                <?php endif;?>
            </div>
        <?php endforeach;?>
    </div>

<?php endif;?>
