<aside class="faq-sidebar my-md-0 mt-3">
    <h3 class="body-large-size">Popular FAQs</h3>
    <hr class="hr-70 ml-0">

    <ul class="popular-faqs">

        <?php $args = array(
            'number_posts' => 6,
            'post_type' => 'faq',
            'post_status' => 'publish',
            'meta_key' => 'wpb_post_views_count',
            'orderby' => 'meta_value_num',
            'order' => 'DESC',
          );

          $pop_faqs = get_posts($args);
          foreach ($pop_faqs as $pop_faq):
        ?>

            <li>
                <a href="<?php echo get_permalink($pop_faq->ID);?>"><?php echo $pop_faq->post_title;?></a>
            </li>

        <?php endforeach;?>
    </ul>

    <div class="sidebar-chat-box">
        <img src="<?php asset_uri('images/icons/chat-icon.svg');?>" class="mb-0_5">
        <p class="ff-mont body-large-size colour-white">Can't find what you are looking for? Speak to one of our experts today.</p>
        <div data-id="Ve5umLPFXQ7" class="livechat_button"><a href="https://www.livechatinc.com/?utm_source=chat_button&utm_medium=referral&utm_campaign=lc_9688570">Start a chat</a></div>
    </div>
</aside>