<?php $case_studies = get_field('featured_case_studies');
if($case_studies):?>

<section class="featured-case-studies my-2 my-md-3">
    <div class="container">
        <div class="text-center">
            <h3 class="section-title">Case Studies</h3>
            <hr class="hr-70">
        </div>
        <div class="row">
            <?php foreach($case_studies as $case_study):
                $title = $case_study['case_study']->post_title;
                $link = get_permalink($case_study['case_study']->ID);
                $image = get_the_post_thumbnail_url($case_study['case_study']->ID, 'gallery_medium');
                $category = get_the_category($case_study['case_study']->ID);
            ?>
                <div class="col-sm-6 mb-1 mb-sm-0">
                    <a class="d-block" href="<?php echo $link;?>">
                        <div class="case-study-image lozad" data-background-image="<?php echo $image;?>">
                            <span class="category-badge cat-<?php echo $category[0]->slug;?>"><?php echo $category[0]->name;?></span>
                        </div>
                        <div class="case-study-details p-1">
                            <h3 class="mb-0"><?php echo $title;?></h3>
                        </div>
                    </a>
                </div>
            <?php endforeach;?>
        </div>
    </div>
</section>

<?php endif; ?>