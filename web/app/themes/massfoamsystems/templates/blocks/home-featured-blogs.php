<?php $blogs = get_field('featured_blogs');
if($blogs):?>

<section class="featured-blogs my-2 my-md-3">
    <div class="container">
        <div class="text-center">
            <h3 class="section-title">Featured Blogs</h3>
            <hr class="hr-70">
        </div>
        <div class="row">
            <?php foreach($blogs as $blog):
                $title = $blog['blog']->post_title;
                $link = get_permalink($blog['blog']->ID);
                $image = get_the_post_thumbnail_url($blog['blog']->ID, 'gallery_medium');
                $category = get_the_category($blog['blog']->ID);
            ?>
                <div class="col-sm-6 mb-1 mb-sm-0">
                    <a class="d-block" href="<?php echo $link;?>">
                        <div class="featured-blog-image lozad" data-background-image="<?php echo $image;?>">
                            <span class="category-badge cat-<?php echo $category[0]->slug;?>"><?php echo $category[0]->name;?></span>
                        </div>
                        <div class="featured-blog-details p-1">
                            <h3 class="mb-0"><?php echo $title;?></h3>
                        </div>
                    </a>
                </div>
            <?php endforeach;?>
        </div>
    </div>
</section>

<?php endif; ?>