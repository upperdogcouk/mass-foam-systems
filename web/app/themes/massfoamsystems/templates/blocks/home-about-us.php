<?php
$ourCompanyH2 = get_field('our_company_header');
$ourCompanyText = get_field('our_company_text');
$ourCompanyImage = get_field('our_company_image');
?>

<?php if ($ourCompanyText && $ourCompanyH2 && $ourCompanyImage): ?>
  <section class="home-about-us py-2 py-md-3">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-6 text-center">
          <img class="img-fluid lozad" data-src="<?php echo esc_url($ourCompanyImage['url']); ?>" alt="<?php echo esc_attr($ourCompanyImage['alt']); ?>" data-title="<?php echo esc_attr($ourCompanyImage['title']); ?>">
        </div>
        <div class="col-md-6 pl-md-1_5">
          <h2><?php echo $ourCompanyH2; ?></h2>
          <p><?php echo $ourCompanyText; ?></p>
          <a href="<?php echo get_permalink( get_page_by_path( 'about-mass-foam-systems' ) );?>" class="mf-button">About Us</a>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>