<?php $about_grid = get_field('about_image_text_grid');
if($about_grid):?>

<section class="about-grid my-xl-3">
    <div class="container">
        <div class="grid-grey-bg">

            <?php foreach($about_grid as $ag):
              $image = $ag['image'];
              $title = $ag['text_title'];
              $paragraph = $ag['text_paragraph'];
              $orientation = $ag['orientation'];
            ?>
                <div class="row no-gutters align-items-center <?php if($orientation) echo 'flex-sm-row-reverse';?>">
                    <div class="col-sm-6">
                        <div class="about-grid-text">
                            <?php if($title):?>
                                <h2 class="colour-sea-green"><?php echo $title;?></h2>
                            <?php endif;?>
                            <p class="mb-0"><?php echo $paragraph;?>    </p>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <?php echo wp_get_attachment_image($image['ID'], 'medium_large', '', array('class' => 'w-100 img-fluid'));?>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    </div>
</section>

<?php endif; ?>