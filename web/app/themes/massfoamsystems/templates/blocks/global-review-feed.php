<?php
$reviews = get_field('what_our_customers_say', 'option');
?>

<section class="global-review-feed my-2 my-md-3">
    <div class="wide-container">
    <!-- <div class="container"> -->

        <h3 class="section-title text-center"><?php echo (is_front_page() ? 'What our customers say' : 'Our Reviews'); ?></h3>
        <hr class="hr-70">

        <?php if ($reviews) : ?>
            <div class="review-slider">
                <?php foreach ($reviews as $review) :
                    $review_title = $review['review_title'];
                    $review_date = $review['review_date'];
                    $review_author = $review['review_author'];
                    $review_star_rating = $review['review_star_rating'];
                    $review_text = $review['review_text'];
                    $review_link = $review['review_link'];
                ?>

                    <div class="review-slide text-center">
                        <a href="<?php echo $review_link; ?>" class="p-1 h-100" target="_blank" rel="noopener noreferrer">
                            <div class="review-star-rating">
                                <h4 class="review-title mb-1">"<?php echo $review_title; ?>"</h4>
                                <div class="review-stars align-content-center mb-1">
                                    <?php if ($review_star_rating == '5 Stars') { ?>
                                        <img src="<?php asset_uri('images/stars/5stars.svg'); ?>" alt="5 star rating" class="5-star m-auto">
                                    <?php } elseif ($review_star_rating == '4 Stars') { ?>
                                        <img src="<?php asset_uri('images/stars/4stars.svg'); ?>" alt="4 star rating" class="4-star m-auto">
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="slide-details">
                                <p class="review-text mb-1_5"><?php echo mb_strimwidth($review_text, 0, 200, '...'); ?></p>
                                <p class="review-date mb-0"><?php echo $review_date; ?></p>
                                <?php if ($review_author) : ?>
                                    <p class="review-author mb-0"><?php echo $review_author; ?></p>
                                <?php endif; ?>
                            </div>
                        </a>
                    </div>

                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <div class="mt-1">
            <!-- TrustBox widget - Review Collector -->
            <div class="trustpilot-widget text-center">
                <a href="https://uk.trustpilot.com/review/www.massfoamsystems.co.uk" target="_blank" rel="noopener noreferrer">
                    <img src="<?php asset_uri('images/trustpilot-button.png'); ?>" alt="Review us on Trustpilot">
                </a>
            </div>
            <!-- End TrustBox widget -->
        </div>
    </div>
</section>