<?php
  $args = array(
    'numberposts' => 3,
    'offset' => 0,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => 'post',
    'post_status' => 'publish',
    'post__not_in' => array( $post->ID ),
    'category' => get_the_category()[0]->term_id
  );

  $related_posts = wp_get_recent_posts( $args, ARRAY_A );
  if($related_posts):
?>

<section class="blog-related-posts my-2 my-md-3">
    <div class="container">

        <div class="text-center">
            <h3 class="section-title">Related Blogs</h3>
            <hr class="hr-70">
        </div>

        <div class="row blog-posts-list">

            <?php foreach ($related_posts as $rp_i => $rp_v):
                $category = get_the_category($rp_v['ID']);
                $image =
                setup_postdata($rp_v);
            ?>

                <div class="col-sm-6 col-md-4 mb-1 mb-sm-0 blog-col">
                    <div class="blog-card">
                        <div class="blog-image">
                            <a href="<?php echo get_the_permalink($rp_v['ID']);?>">
                                <img class="img-fluid w-100 lozad" src="<?php asset_uri('images/blog-no-image.png');?>" data-src="<?php echo (get_the_post_thumbnail($rp_v['ID']) ? get_the_post_thumbnail_url($rp_v['ID'], 'blog_430') : get_asset_uri('images/blog-no-image.png'));?>" alt="mass foam system logo">
                            </a>
                            <span class="category-badge cat-<?php echo $category[0]->slug;?>"><?php echo $category[0]->name;?></span>
                        </div>

                        <div class="p-0_75">
                            <h3 class="mb-0"><?php echo get_the_title($rp_v['ID']);?></h3>
                            <p class="byline author vcard body-smallest-size mb-0_5">by <?php echo get_the_author();?></p>
                        </div>

                        <div class="row no-gutters link-date justify-content-between">
                            <div class="col-auto py-0_5 px-0_75">
                                <a href="<?php echo get_the_permalink($rp_v['ID']);?>" class="mf-text-button">Read More</a>
                            </div>
                            <div class="col-auto py-0_5 px-0_75">
                                <time datetime="<?php echo get_post_time('c', true, $rp_v['ID']);?>" class="colour-boulder m-0 body-small-size updated">
                                    <?php echo get_post_time('l j F Y', true, $rp_v['ID']);?>
                                </time>
                            </div>
                        </div>
                    </div>
                </div>

            <?php wp_reset_postdata(); endforeach ?>

        </div>
    </div>
</section>

<?php endif; ?>