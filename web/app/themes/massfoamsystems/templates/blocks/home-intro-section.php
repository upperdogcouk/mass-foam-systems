<?php
$home_intro_heading = get_field('home_intro_heading');
$home_intro_text = get_field('home_intro_text');
$home_intro_image = get_field('home_intro_image');
$home_intro_button = get_field('home_intro_button');
if ($home_intro_heading) :
?>
    <section class="home-intro-section py-2 py-md-3">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 pr-md-1_5 mb-1 mb-md-0">
                    <h2><?php echo $home_intro_heading; ?></h2>
                    <p><?php echo $home_intro_text; ?></p>
                    <div class="d-flex align-items-center">
                        <div class="bg-white rounded p-0_5 mr-0_25 mb-0_25 mb-md-0 mw-50">
                            <img data-src="<?php echo asset_uri("images/pas-certified.png"); ?>" class="img-fluid lozad" alt="PAS certified" style="max-height: 60px;" />
                        </div>
                        <div class="bg-white rounded p-0_5 mb-0_25 mb-md-0 mw-50">
                            <img data-src="<?php echo asset_uri("images/trustmark-logo.jpg"); ?>" class="img-fluid lozad" alt="Trustmark certified" style="max-height: 60px;" />
                        </div>
                    </div>
                    <?php if ($home_intro_button) : ?>
                        <a class="mf-button mr-1" href="<?php echo get_permalink(get_page_by_path('what-is-icynene-spray-foam/')); ?>">What's Icynene?</a>
                    <?php endif; ?>
                </div>
                <div class="col-md-6 text-center">
                    <img id="home-intro-img" class="img-fluid lozad" data-src="<?php echo esc_url($home_intro_image['url']); ?>" alt="<?php echo esc_attr($home_intro_image['alt']); ?>" data-title="<?php echo esc_attr($home_intro_image['title']); ?>">
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>