<?php
$our_sectors = get_field('our_sectors');
if ($our_sectors):
    ?>
    <section class="home-our-sectors pt-2 pt-md-3 mb-2 mb-md-3">
        <div class="container">
            <h3 class="section-title text-center">Our Sectors</h3>
            <hr class="hr-70">
            <div class="row">
                <?php foreach ($our_sectors as $sector):
                    $sector_title = $sector['sector_title'];
                    $sector_image = $sector['sector_image'];
                    $sector_text = $sector['sector_text'];
                    $sector_link = $sector['sector_link'];
                    $sector_button = $sector['sector_button'];
                    $sector_width = ($sector['sector_width'] ? 'col-sm-6 col-md-7' : 'col-sm-6 col-md-5');
                ?>
                    <div class="mb-1 <?php echo $sector_width ?>">
                        <div class="sector-card d-flex h-100 flex-column">
                            <a href="<?php echo $sector_link ?>" class="sector-image lozad" data-background-image="<?php echo $sector_image['sizes']['medium_large']; ?>"></a>
                            <div class="sector-card-details p-1">
                                <a href="<?php echo $sector_link ?>"><h3 class="mb-0 mb-sm-0_5"><?php echo $sector_title ?></h3></a>
                                <p class="m-0 d-none d-sm-inline-block"><?php echo $sector_text ?></p>
                            </div>
                            <div class="sector-card-link mt-auto px-1 py-0_75">
                                <a class="mf-text-button" href="<?php echo $sector_link ?>"><?php echo $sector_button ?></a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
<?php endif; ?>