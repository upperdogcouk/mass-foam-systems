<?php
$home_selection = get_field('home_selection');
$title = get_field('home_selection_title');
$caption = get_field('home_selection_caption');
$big_option = get_field('home_selection_big_option');

if ($home_selection) : ?>
    <section class="home-selection">
        <div class="home-main">
            <h1><?php echo $title; ?></h1>
            <?php if ($caption) : ?>
                <span><?php echo $caption; ?></span>
            <?php endif; ?>
            <!-- TrustBox widget - Micro Combo -->
            <!-- <div class="trustpilot-widget" data-locale="en-GB" data-template-id="5419b6ffb0d04a076446a9af" data-businessunit-id="5ad8867d40ff7f0001ef011b" data-style-height="20px" data-style-width="100%" data-theme="light">
        <a href="https://uk.trustpilot.com/review/www.massfoamsystems.co.uk" target="_blank" rel="noopener">Trustpilot</a>
      </div> -->
            <!-- End TrustBox widget -->
        </div>

        <?php if (count(array_filter($big_option)) > 0) : ?>
            <div class="box-selection-container mt-1">
                <a href="<?php echo $big_option['box_link']; ?>" class="big-option">
                    <div class="box-option" style="background-image: url('<?php echo $big_option['background_image']['sizes']['gallery_medium']; ?>')">
                        <div class="box-content-container">
                            <span class="box-content-header"><?php echo $big_option['box_header']; ?></span>
                            <?php if ($big_option['box_text']) : ?>
                                <span><?php echo $big_option['box_text']; ?></span>
                            <?php endif; ?>
                        </div>
                        <div class="box-arrow-container">
                            <img src="<?php asset_uri('images/icons/arrow-right.svg'); ?>" alt="right arrow">
                        </div>
                    </div>
                </a>
            </div>
        <?php endif; ?>

        <div class="box-selection-container">
            <?php foreach ($home_selection as $box_i => $box_k) :
                $bg_image = $box_k['background_image'];
                $box_header = $box_k['box_header'];
                $box_text = $box_k['box_text'];
                $box_link = $box_k['box_link'];
            ?>
                <a href="<?php echo $box_link; ?>">
                    <div class="box-option" style="background-image: url('<?php echo $bg_image['sizes']['gallery_medium']; ?>')">
                        <div class="box-content-container">
                            <span class="box-content-header"><?php echo $box_header; ?></span>
                            <?php if ($box_text) : ?>
                                <span><?php echo $box_text; ?></span>
                            <?php endif; ?>
                        </div>
                        <div class="box-arrow-container">
                            <img src="<?php asset_uri('images/icons/arrow-right.svg'); ?>" alt="right arrow">
                        </div>
                    </div>
                </a>
            <?php endforeach; ?>
        </div>
        <div class="bubble-overlay"></div>
    </section>
<?php endif; ?>