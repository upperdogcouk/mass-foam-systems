<?php
$worked_with = get_field('worked_with', 'option');
?>
<section class="home-worked-with my-2 my-md-3">
  <div class="container">

    <h3 class="section-title text-center">Who We've Worked With</h3>
    <hr class="hr-70">

    <?php if ($worked_with) : ?>
      <div class="worked-with-logos px-1 py-0_5 p-md-1">
        <div class="row align-items-center">
          <?php foreach ($worked_with as $company) :
            $company_name = $company['company_name'];
            $company_image = $company['company_image'];
            $company_link = $company['company_link'];
          ?>
            <div class="worked-with-logo text-center">
              <a href="<?php echo $company_link ?>" rel="nofollow noopener">
                <img data-lazy="<?php echo $company_image ?>" alt="<?php echo $company_name ?>" class="d-inline" style="max-height: 80px; max-width: 160px; margin: 0 20px">
              </a>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    <?php endif; ?>
  </div>
</section>