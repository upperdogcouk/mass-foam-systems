<?php
    $show_footer_cta = get_field('show_footer_cta');
    $footer_cta_header = get_field('footer_cta_header');
    $footer_cta_link = get_field('footer_cta_link');
    $footer_cta_link_text = get_field('footer_cta_link_text');
    $footer_cta_background = (get_field('footer_cta_background') ? get_field('footer_cta_background')['sizes']['hero_large_1920'] : get_asset_uri('images/default-footer-cta-bg.jpg'));

    if ($show_footer_cta):
?>
    <section class="global-footer-contact-cta py-2 py-md-4 lozad" data-background-image="<?php echo $footer_cta_background; ?>">

        <div class="container">
            <div class="row justify-content-center justify-content-md-between align-items-center">
                <div class="col-sm-10 col-md-8 col-lg-6 text-center text-md-left">
                    <h2 class="mb-1"><?php echo $footer_cta_header ?></h2>

                    <a class="mf-button mr-1" title="<?php echo $footer_cta_link_text; ?>"
                       href="<?php echo($footer_cta_link ? $footer_cta_link['url'] : get_permalink(39)); ?>">
                        <?php echo $footer_cta_link_text; ?>
                    </a>
                    <a class="mf-button white-button mr-1" title="Call us" href="tel:08002465051">Call us <span
                                class="hide-mob">now on 0800 246 5051</span></a>
                </div>

                <div class="col-xs-6 col-md-3">
                    <div class="trustpilot-footer p-1 mt-2 mt-md-0 text-center">
                        <div class="trustpilot-stars align-content-center mt-auto mb-1">
                            <img src="<?php asset_uri('images/stars/5stars.svg'); ?>" alt="5 star rating" class="5-star img-fluid m-auto">
                        </div>
                        <!-- TrustBox widget - Micro Review Count -->
                        <div class="trustpilot-widget align-content-center mt-auto" data-locale="en-GB" data-template-id="5419b6a8b0d04a076446a9ad"
                             data-businessunit-id="5ad8867d40ff7f0001ef011b" data-style-height="50px"
                             data-style-width="100%"
                             data-theme="light">
                            <a href="https://uk.trustpilot.com/review/www.massfoamsystems.co.uk" target="_blank" rel="noopener noreferrer">Trustpilot</a>
                        </div>
                        <!-- End TrustBox widget -->
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>