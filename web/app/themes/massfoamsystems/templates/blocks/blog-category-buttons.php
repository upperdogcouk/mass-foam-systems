<?php
    $cats = get_terms(array('taxonomy' => 'category', 'hide_empty' => false));
    if($cats):

      // Get the ID of the current category archive so we can check if it matches below to add .active class
    $current_cat_ID = get_queried_object()->term_taxonomy_id;
?>
    <ul class="blog-category-buttons">
        <?php foreach($cats as $cat):?>
            <li><a class="cat-<?php echo $cat->slug; if($current_cat_ID == $cat->term_taxonomy_id) echo ' active';?>" href="<?php echo get_term_link($cat);?>"><?php echo $cat->name;?></a></li>
        <?php endforeach;?>
    </ul>
<?php endif; ?>