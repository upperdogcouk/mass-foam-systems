<?php
    if (is_home()) {
      $field_ID = get_option( 'page_for_posts' );
    } else {
      $field_ID = null;
    }

    $hero_img = get_field('hero_image', $field_ID);

    if(is_home() || is_singular('post')) {
        $subtitle = get_field('page_subtitle', $field_ID) ? get_field('page_subtitle', $field_ID) : 'Mass Foam Blog';
    } elseif (is_post_type_archive('faq') || is_singular('faq')) {
        $subtitle = get_field('page_subtitle', $field_ID) ? get_field('page_subtitle', $field_ID) : 'FAQs';
    } elseif (is_404()) {
      $subtitle = '404';
    } else {
      $subtitle = get_field('page_subtitle', $field_ID);
    }
?>

<?php if($hero_img):?>
<style>
  .page-hero-image {background-image: url("<?php echo $hero_img['sizes']['medium'];?>");}

  @media (min-width: 576px) {
    .page-hero-image {background-image: url("<?php echo $hero_img['sizes']['medium_large'];?>");}
  }

  @media (min-width: 768px) {
    .page-hero-image {background-image: url("<?php echo $hero_img['sizes']['hero_medium_1280'];?>");}
  }

  @media (min-width: 1280px) {
    .page-hero-image {background-image: url("<?php echo $hero_img['sizes']['hero_large_1920'];?>");}
  }
</style>
<?php endif;?>

<section class="page-hero-image">
    <?php if($subtitle):?>
        <div class="container">
            <p class="page-subtitle"><?php echo $subtitle;?></p>
            <hr class="hr-70-white mt-0_5">
        </div>
    <?php endif;?>
</section>
