<?php
$benefits_title_column_1 = get_field('benefits_title_column_1');
$benefits_column_1 = get_field('benefits_column_1');

$benefits_title_column_2 = get_field('benefits_title_column_2');
$benefits_column_2 = get_field('benefits_column_2');
if ($benefits_column_2 || $benefits_column_2):
    ?>
    <section class="home-benefits-of-icynene my-2 my-md-3 py-1_5 py-md-3">
        <div class="container">
            <div class="row">
                <?php if ($benefits_column_1): ?>
                    <div class="col-md-6 my-1 my-md-0">
                        <div class="benefit-card d-flex h-100 flex-column text-center home">
                            <div class="benefit-title p-1">
                                <h4 class="m-0"><?php echo $benefits_title_column_1 ?></h4>
                            </div>
                            <div class="benefit-list p-1">
                                <ul class="home-benefits">
                                    <?php foreach ($benefits_column_1 as $benefit):
                                        $benefit_bullet_column_1 = $benefit['benefit_bullet_column_1']; ?>
                                        <li class="py-0_5"><span class="tick align-middle"><img class="py-0_5 pr-0_5" src="<?php asset_uri('images/icons/tick.svg');?>" alt="tick"></span><?php echo $benefit_bullet_column_1 ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                            <div class="benefit-link mt-auto p-1">
                                <a class="mf-button" href="<?php echo get_permalink( get_page_by_path( 'request-a-call-back' ) );?>">Request a call back</a>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if ($benefits_column_2): ?>
                    <div class="col-md-6 my-1 my-md-0">
                        <div class="benefit-card d-flex h-100 flex-column text-center business">
                            <div class="benefit-title p-1">
                                <h4 class="m-0"><?php echo $benefits_title_column_2 ?></h4>
                            </div>
                            <div class="benefit-list p-1">
                                <ul class="business-benefits">
                                    <?php foreach ($benefits_column_2 as $benefit):
                                        $benefit_bullet_column_2 = $benefit['benefit_bullet_column_2']; ?>
                                        <li class="py-0_5"><img class="tick align-middle py-0_5 pr-0_5" src="<?php asset_uri('images/icons/tick.svg');?>" alt="tick"> <?php echo $benefit_bullet_column_2 ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                            <div class="benefit-link mt-auto p-1">
                                <a class="mf-button" href="<?php echo get_permalink( get_page_by_path( 'request-a-call-back' ) );?>">Request a call back</a>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </section>
<?php endif; ?>