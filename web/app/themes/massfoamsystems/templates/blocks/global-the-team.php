<?php
$the_team = get_field('the_team', 'option');
?>
<div class="container">
  <?php if ($the_team) : ?>
    <div class="the-team px-1 py-0_5 p-md-1">
      <div class="row align-items-center">
        <?php foreach ($the_team as $member) :
          $member_image = $member['image'];
          $member_name = $member['name'];
          $member_description = $member['description'];
        ?>
          <div class="team-member text-center p-1">
            <?php if ($member_image) : ?>
              <img data-lazy="<?php echo $member_image ?>" alt="<?php echo $member_name ?>" class="d-inline" />
            <?php endif; ?>
            <div>
              <h4 class="mt-0_5"><b><?php echo $member_name ?></b></h4>
              <?php if ($member_description) : ?>
                <p class="m-0"><?php echo $member_description ?></p>
              <?php endif; ?>
            </div>
          </div>
        <?php endforeach ?>
      </div>
    </div>
  <?php endif; ?>
</div>