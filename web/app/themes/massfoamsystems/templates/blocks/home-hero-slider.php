<?php
$home_slider = get_field('home_hero_slider');
if ($home_slider) :
?>

    <section class="home-hero-slider">
        <?php foreach ($home_slider as $slide_i => $slide_k) :
            $bg_type = $slide_k['background_type'];
            $vid_snip = $slide_k['slide_video_snippet'];
            $vid = $slide_k['slide_video_youtube'];
            if ($vid) {
                $video_id = parse_video_uri($vid)['id'];
            }
            $bg_img = $slide_k['background_image'];
            $roundel = $slide_k['roundel_text'];
            $main_text = $slide_k['main_text'];
            $sub_text = $slide_k['sub_text'];
            $green_l = $slide_k['green_link'];
            $bubble_overlay = $slide_k['bubble_overlay'];
            $show_button = $slide_k['show_button'];
            $button_text = $slide_k['button_text'];
            $button_link = $slide_k['button_link'];
        ?>

            <div class="slide slide-<?php echo $slide_i; ?> bg-<?php echo $bg_type; ?>">
                <?php if ($bg_type == 'video') { ?>
                    <div class="slide-video-wrapper">
                        <video autoplay muted loop preload src="<?php echo $vid_snip; ?>" playsinline></video>
                        <button class="slide-video-trigger" data-vid="<?php echo $video_id; ?>" data-toggle="modal" data-target="#Modal-<?php echo $slide_i; ?>">Watch our video</button>
                    </div>
                <?php } elseif ($bg_type == 'image') { ?>
                    <style>
                        .slide-<?php echo $slide_i; ?> {
                            background-color: black;
                        }

                        .slide-<?php echo $slide_i; ?>>.slide-image-holder {
                            background-image: url("<?php echo $bg_img['sizes']['medium_large']; ?>");
                            opacity: 0.6;
                        }

                        @media screen and (min-width: 767px) {
                            .slide-<?php echo $slide_i; ?>>.slide-image-holder {
                                background-image: url("<?php echo $bg_img['sizes']['hero_medium_1280']; ?>");
                            }
                        }

                        @media screen and (min-width: 1400px) {
                            .slide-<?php echo $slide_i; ?>>.slide-image-holder {
                                background-image: url("<?php echo $bg_img['sizes']['hero_large_1920']; ?>");
                            }
                        }
                    </style>
                    <div class="slide-image-holder"></div>

                <?php } ?>

                <?php if ($bubble_overlay) : ?><div class="slide-bubble-overlay"></div><?php endif; ?>

                <div class="slide-contents">
                    <div class="container">
                        <div class="<?php if ($bg_type != "image") {
                                        echo "col-md-6 text-md-left";
                                    } ?> text-center">
                            <?php if ($roundel) : ?><p class="slide-roundel ff-mont mb-0_75"><?php echo $roundel; ?></p><?php endif; ?>
                            <?php if ($main_text) : ?>
                                <?php if ($slide_i == 0) : ?><h1 class="slide-main ff-mont
                                    <?php if ($sub_text && $bg_type == 'image') {
                                        echo 'slide-main-image';
                                    } ?>
                                "><?php echo $main_text; ?></h1>
                                <?php else : ?><h2 class="slide-main ff-mont
                                    <?php if ($sub_text && $bg_type == 'image') {
                                        echo 'slide-main-image';
                                    } ?>
                                "><?php echo $main_text ?></h2><?php endif; ?>
                            <?php endif; ?>
                            <?php if ($sub_text) : ?>
                                <p class="slide-sub <?php echo $bg_type == "video" ? "slide-sub-video" : ""; ?>"><?php echo $sub_text ?></p>
                            <?php endif; ?>
                            <?php if ($green_l) : ?><a class="mf-button mr-1" title="<?php echo $green_l['title']; ?>" href="<?php echo $green_l['url']; ?>"><?php echo $green_l['title']; ?></a><?php endif; ?>

                            <?php if ($show_button) : ?>
                                <span class="hide-mob">
                                    <a class="mf-button white-button" title="Call us" href="<?php echo $button_link ?>" target="_blank"><?php echo $button_text ?></a>
                                </span>
                            <?php endif; ?>

                            <?php if ($bg_type == 'video') { ?>
                                <button class="mf-button mx-0_25 d-md-none" data-vid="<?php echo $video_id; ?>" data-toggle="modal" data-target="#Modal-<?php echo $slide_i; ?>">Watch our video</button>
                            <?php } ?>

                            <a href="https://www.massfoamsystems.co.uk/instant-quote/" target='_blank' class="mf-button white-button mx-0_25 mt-0_5 d-md-none">
                                <span>INSTANT QUOTE</span>
                            </a>

                            <div class="dots-holder"></div>
                        </div>
                    </div>
                </div>
            </div>

        <?php endforeach; ?>
    </section>

    <?php foreach ($home_slider as $slide_i => $slide_k) :
        $bg_type = $slide_k['background_type']; ?>

        <?php if ($bg_type == 'video') { ?>
            <div class="modal fade video-modal" id="Modal-<?php echo $slide_i; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-video modal-content">
                        <iframe frameborder="0" style="border:0;" width="100%" height="100%" src=""></iframe>
                    </div>
                </div>
            </div>
        <?php } ?>
    <?php endforeach; ?>

<?php endif; ?>