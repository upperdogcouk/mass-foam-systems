<?php
$accreditations = get_field('accreditations', 'option');
?>

<section class="global-accredited-trustworthy my-2 my-md-3">
    <div class="container">

        <h3 class="section-title text-center">We're accredited and trustworthy</h3>
        <hr class="hr-70">

        <?php if ($accreditations): ?>
            <div class="accreditation-logos px-1 py-0_5 p-md-1">
                <div class="row align-items-center">
                    <?php foreach ($accreditations as $accreditation):
                        $accreditation_logo = $accreditation['accreditation_logo'];
                        $accreditation_name = $accreditation['accreditation_name'];
                    ?>
                        <div class="accreditation-logo text-center">
                            <img data-lazy="<?php echo $accreditation_logo ?>" alt="<?php echo $accreditation_name ?>" class="d-inline" style="max-height: 80px; max-width: 160px; margin: 0 20px">
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</section>