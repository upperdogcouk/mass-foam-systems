<?php
    $essex_address = get_field('essex_address', 'option');
    $essex_map = get_field('essex_google_map', 'option');
    $poole_address = get_field('poole_address', 'option');
    $poole_map = get_field('poole_google_map', 'option');
    $poole_phone = get_field('poole_phone','option');
    $essex_phone = get_field('essex_phone','option');
?>

<section class="contact-grid my-2 my-md-3">
    <div class="container">
        <div class="grid-grey-bg">
            <div class="row no-gutters align-items-center">
                <div class="col-sm-6">
                    <div class="about-grid-text">
                        <h3 class="colour-sea-green">Essex</h3>
                        <p><?php echo $essex_address;?></p>

                        <p class="mb-0"><a href="tel:<?php echo $essex_phone;?>"><?php echo $essex_phone;?></a></p>
                        <a href="https://goo.gl/maps/aufqA2a2Bkz" class="mt-0_75 mf-button">Get directions</a>
                    </div>
                </div>
                <div class="col-sm-6">
                    <?php echo $essex_map;?>
                </div>
            </div>

            <div class="row no-gutters align-items-center flex-sm-row-reverse">
                <div class="col-sm-6">
                    <div class="about-grid-text">
                        <h3 class="colour-sea-green">Poole</h3>
                        <p><?php echo $poole_address;?></p>

                        <p class="mb-0"><a href="tel:<?php echo $poole_phone;?>"><?php echo $poole_phone;?></a></p>

                        <a href="https://goo.gl/maps/nH5TpeohTdT2" class="mt-0_75 mf-button">Get directions</a>
                    </div>
                </div>
                <div class="col-sm-6">
                  <?php echo $poole_map;?>
                </div>
            </div>
        </div>
    </div>
</section>