<?php
$whymassfoams = get_field('why_use_mass_foams', 'option');
?>

<section class="global-why-use-mass-foam py-2 py-md-3 text-center">
    <div class="container">

        <h3 class="section-title">Why use Mass Foam?</h3>
        <hr class="hr-70">

        <?php if ($whymassfoams): ?>
            <div class="row no-gutters flex-wrap justify-content-center">
                <?php foreach ($whymassfoams as $benefit):
                    $benefit_icon = $benefit['benefit_icon'];
                    $benefit_title = $benefit['benefit_title'];
                    $benefit_info = $benefit['benefit_info'];
                    ?>
                    <div class="benefit px-0_25 my-0_5" tabindex="0">
                        <div class="benefit-icon">
                            <img src="<?php echo $benefit_icon ?>" alt="<?php echo $benefit_title ?>">
                        </div>
                        <p class="m-0"><?php echo $benefit_title ?></p>
                        <div class="benefit-tooltip">
                            <p class="body-small-size m-0"><?php echo $benefit_info ?></p>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <a class="mf-button mt-1" href="<?php echo get_permalink( get_page_by_path( 'request-a-call-back' ) );?>">Request a call back</a>
    </div>
</section>