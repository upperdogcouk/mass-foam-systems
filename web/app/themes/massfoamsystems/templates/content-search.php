<a href="<?php the_permalink();?>" class="faq-question-card d-block d-sm-flex align-items-start justify-content-between mb-1">
    <p class="mb-0 pr-sm-1"><?php the_title();?></p>
    <span class="mf-text-button">Read More</span>
</a>