<?php while (have_posts()) : the_post(); ?>
    <div class="container">
        <div class="page-content-wrap">
            <div class="page-content">
                <?php get_template_part('templates/page', 'title'); ?>
                <div class="post-meta d-xs-flex justify-content-between mb-0_5">
                    <p class="byline author vcard body-small-size mb-0">by <?php echo get_the_author();?></p>
                    <time datetime="<?php echo get_post_time('c', true);?>" class="colour-boulder m-0 body-small-size updated">
                      <?php echo get_post_time('l j F Y');?>
                    </time>
                </div>
                <span class="category-badge category-inline mb-0_75 cat-<?php echo get_the_category()[0]->slug;?>"><?php echo get_the_category()[0]->name;?></span>
                <?php the_content();?>
                <?php get_template_part('templates/blocks/blog', 'gallery-slider'); ?>

                <a href="<?php echo get_permalink( get_option( 'page_for_posts' ) );?>" class="mf-button light-grey-button mt-1">Back to blogs</a>
            </div>
        </div>
    </div>
<?php endwhile; ?>
