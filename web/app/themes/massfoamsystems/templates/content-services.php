<?php
    $service_material_types_blurb = get_field('service_material_types_blurb');
    $service_material_type = get_field('service_material_type');
    $why_use_service_header = get_field('why_use_service_header');
    $why_use_service_blurb = get_field('why_use_service_blurb');
    $why_use_services_list = get_field('why_use_services_list');
    $service_video = get_field('service_video');
    $service_gallery = get_field('service_gallery');
?>
<div class="container">
    <div class="page-content-wrap mb-0">
        <div class="page-content">
            <?php get_template_part('templates/page', 'title'); ?>
            <?php the_content(); ?>
            <?php get_template_part('templates/content', 'expanded'); ?>
        </div>
    </div>
    <?php if ($service_material_type): ?>
        <div class="page-content-wrap service-material-types mb-0 mt-0">

            <div class="page-content text-center">
                <h3 class="section-title">Material Types</h3>
                <hr class="hr-70">
                <?php if ($service_material_types_blurb): ?>
                    <p><?php echo $service_material_types_blurb ?></p>
                <?php endif; ?>
                <div class="row justify-content-center">
                    <?php foreach ($service_material_type as $service_material_type_el):
                        $service_material_type_image = $service_material_type_el ['service_material_type_image'];
                        $service_material_type_title = $service_material_type_el ['service_material_type_title'];
                    ?>
                        <div class="col-6 col-sm mb-1">
                            <div class="service-material-type-ind lozad" data-background-image="<?php echo $service_material_type_image['sizes']['medium']; ?>">
                                <p class="text-uppercase"><?php echo $service_material_type_title ?></p>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <img class="bba-cert pt-0_5" src="<?php asset_uri('images/BBA.svg');?>" alt="BBA tick">
            </div>
        </div><!-- Outer bordered box -->
    <?php endif; ?>

    <div class="page-content-wrap why-service-blocks mb-2 mb-md-3">
        <div class="row no-gutters">
            <div class="col-md-6 d-flex flex-column">
                <div class="why-use-service">
                    <h3><?php echo $why_use_service_header ?></h3>
                    <p><?php echo $why_use_service_blurb ?></p>
                </div>
                <div class="service-video mt-auto">
                    <?php echo $service_video ?>
                </div>
            </div>
            <div class="col-md-6">
                <?php if ($why_use_services_list): ?>
                    <div class="why-use-service-list h-100">
                        <ul class="service-list">
                            <?php foreach ($why_use_services_list as $why_use_services_list_el):
                                $why_use_services_list_item = $why_use_services_list_el ['why_use_services_list_item']; ?>
                                <li><?php echo $why_use_services_list_item ?></li>
                            <?php endforeach; ?>
                        </ul>
                        <a class="mf-button mt-1" href="<?php echo get_permalink( get_page_by_path( 'request-a-call-back' ) ); ?>">Request a call back</a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>


    <h3 class="section-title text-center">Gallery</h3>
    <hr class="hr-70">
    <div class="gallery-content">

        <?php
            $images = get_field('service_gallery');
            if ($images):
        ?>
            <div class="row justify-content-center d-none d-sm-flex">
                <?php foreach ($images as $image): ?>
                    <div class="col-sm-6 col-lg-4 mb-1">
                        <a href="<?php echo $image['sizes']['medium_large']; ?>" data-toggle="lightbox" data-gallery="lightbox-gallery" data-footer="<?php echo $image['caption']; ?>">
                            <img class="img-fluid w-100 lozad" src="<?php asset_uri('images/blog-no-image.png') ?>" data-src="<?php echo $image['sizes']['gallery_thumb']; ?>" alt="<?php echo $image['alt']; ?>"/>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>

            <div class="services-gallery-slider d-sm-none">
              <?php foreach ($images as $image): ?>
                  <div class="gallery-slide">
                      <img class="img-fluid w-100 lozad" src="<?php asset_uri('images/blog-no-image.png') ?>" data-src="<?php echo $image['sizes']['gallery_medium']; ?>" alt="<?php echo $image['alt']; ?>"/>
                  </div>
              <?php endforeach; ?>
            </div>


        <?php endif; ?>
    </div>
</div>