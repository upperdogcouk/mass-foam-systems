<?php

$quote_calculator = new QuoteCalculator();

?>

<div class="container">
    <div class="page-content-wrap">
        <div class="page-content">
            <?php get_template_part('templates/page', 'title'); ?>
            <?php if(get_the_content()):?>
                <div class="mb-1">
                    <?php the_content(); ?>
                    <hr>
                </div>
            <?php endif;?>

            <div class="gform_wrapper mfs-form">


                <div id="quote-step-1" class="calc-step active mt-1">
                    <form>
                        <input type="hidden" name="step" value="1">
                        <h3 class="body-large-size">1. A bit about you</h3>
                        <hr class="hr-70 ml-0 mt-0_5">
                        <ul>
                            <li>
                                <label class="small-label">Your name<sup>*</sup></label>
                                <input name="name" type="text" placeholder="Your Name" required>
                            </li>
                            <li>
                                <label class="small-label">Your email address<sup>*</sup></label>
                                <input name="email" type="email" placeholder="Email Address" required>
                            </li>
                            <li>
                                <div class="row align-items-center">
                                    <div class="col-sm-6 col-lg-4">
                                        <label class="small-label">Your telephone number<sup>*</sup></label>
                                        <input name="phone" type="tel" placeholder="Telephone" required>
                                    </div>
                                    <div class="col-sm">
                                        <div class="form-explainer mb-1 mb-sm-0 mt-sm-1">
                                            <p><strong>Why do we need your phone number?</strong><br>
                                                Don’t worry, we <em>never</em> use call centers and will never cold call. However it’s important to talk to our customers to tailor our quotes perfectly to their needs. By clicking submit you agree that one of our insulation specialists may get in touch with you about your quote.</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row align-items-center">
                                    <div class="col-sm-6 col-lg-4">
                                        <label class="small-label">Your postcode<sup>*</sup></label>
                                        <input name="postcode" type="text" placeholder="Postcode" required>
                                    </div>
                                    <div class="col-sm">
                                        <div class="form-explainer mb-1 mb-sm-0 mt-sm-1">
                                            <p><strong>Why do we need your postcode?</strong><br>
                                                We need your postcode so we can determine which friendly local specialist to put in touch with you.
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="mt-0_75">
                                <input type="checkbox" name="privacy_agreement" required><label class="body-small-size">I confirm that by submitting this form I am agreeing to the <a href="/privacy-policy">Privacy Policy</a> </label>
                            </li>
                        </ul>
                        <a class="mf-button quote-submit mt-1_5" id="step-1-submit" href="#">Next ></a>
                    </form>
                </div><!-- /#step-1 -->

                <div id="quote-step-2" class="calc-step mt-2">
                    <form>
                        <input type="hidden" name="step" value="2">
                        <h3 class="body-large-size">2. Select the sector that best fits your needs</h3>
                        <hr class="hr-70 ml-0 mt-0_5">

                        <div class="row">
                            <div class="col-6 col-md radio-choice">
                                <input type="radio" name="sector_radio" value="Residential" id="residential" onclick="document.getElementById('sector').value = this.value;">
                                <label for="residential">Residential</label>
                            </div>
                            <div class="col-6 col-md radio-choice">
                                <input type="radio" name="sector_radio" value="Commercial" id="commercial" onclick="document.getElementById('sector').value = this.value;">
                                <label for="commercial">Commercial</label>
                            </div>
                            <div class="col-6 col-md radio-choice">
                                <input type="radio" name="sector_radio" value="Marine" id="marine" onclick="document.getElementById('sector').value = this.value;">
                                <label for="marine">Marine</label>
                            </div>
                            <div class="col-6 col-md radio-choice">
                                <input type="radio" name="sector_radio" value="Containers" id="containers" onclick="document.getElementById('sector').value = this.value;">
                                <label for="containers">Containers</label>
                            </div>
                            <div class="col-6 col-md radio-choice">
                                <input type="radio" name="sector_radio" value="Other" id="other" onclick="document.getElementById('sector').value = this.value;">
                                <label for="other">Other</label>
                            </div>
                        </div>
                        <input type="hidden" name="sector" id="sector">
                    </form>
                </div><!-- /#step-2 -->

                <div id="quote-step-3" class="calc-step mt-2">
                    <form>
                        <input type="hidden" name="step" value="3">

                        <div class="3-residential-message">
                            <h3 class="body-large-size">3. How big is your property</h3>
                            <hr class="hr-70 ml-0 mt-0_5">
                            <div class="row">
                                <div class="col-sm-6 col-md property-choice">
                                    <input type="radio" name="property-size" value="50-70sqm" id="50-70sqm" onclick="document.getElementById('size_of_property').value = this.value;">
                                    <label for="50-70sqm">
                                        <img src="<?php asset_uri('images/property-small.svg');?>" alt="small-sized property"><br>
                                        <span class="large">50 - 70</span>
                                        <br>square meters
                                    </label>
                                </div>
                                <div class="col-sm-6 col-md property-choice">
                                    <input type="radio" name="property-size" value="70-90sqm" id="70-90sqm" onclick="document.getElementById('size_of_property').value = this.value;">
                                    <label for="70-90sqm">
                                        <img src="<?php asset_uri('images/property-medium.svg');?>" alt="medium-sized property"><br>
                                        <span class="large">70 - 90</span>
                                        <br>square meters
                                    </label>
                                </div>
                                <div class="col-sm-6 col-md property-choice">
                                    <input type="radio" name="property-size" value="90+sqm" id="90+sqm" onclick="document.getElementById('size_of_property').value = this.value;">
                                    <label for="90+sqm">
                                        <img src="<?php asset_uri('images/property-large.svg');?>" alt="large-sized property"><br>
                                        <span class="large">90 +</span>
                                        <br>square meters
                                    </label>
                                </div>
                            </div>
                            <div class="text-center dont-know-choice">
                                <input type="radio" name="property-size" value="Dont Know" id="property-dont-know" onclick="document.getElementById('size_of_property').value = this.value;">
                                <label for="property-dont-know">I don't know</label>
                            </div>
                            <input type="hidden" name="size_of_property" id="size_of_property">
                        </div>


                        <div id="quote-survey-req" class="3-other-message mt-2" style="display: none">
                            <!--<h3 class="body-large-size">3. Your selection requires a site survey to provide an accurate quote.</h3>-->
                            <h3 class="body-large-size">3. We have temporarily disabled instant quotes.</h3>
                            <hr class="hr-70 ml-0 mt-0_5">

                            <!-- Temp changes to disable quotations -->
                            <p>Instant quotes have temporarily been disabled. Do not worry, we will resolve this ASAP.</p>

                            <p>Please <a href="tel:08002465051">call us</a> direct to find out more and book your survey or submit the form for us to get in touch.</p>

                            <div id="quote-submit" class="mt-2">
                                <div class="row">
                                    <div class="col-sm-6 mb-0_5 mb-sm-0">
                                        <button type="button" class="mf-button request-callback w-100 text-center">Request a call back</button>
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="tel:08002465051" class="mf-outline-button w-100 text-center">Call us now 0800 246 5051</a>
                                    </div>
                                </div>
                                <p class="body-smallest-size mt-0_5">*All quotes are subject to survey</p>
                            </div><!-- /#quote-submit -->

                            <input type="hidden" name="request_a_callback" id="request_a_callback" value="">

                        </div><!-- #quote-survey-req -->

                    </form>
                </div><!-- /#step-3 -->

                <div id="quote-step-4" class="calc-step mt-2">
                    <form>
                        <div class="d-none"> <!-- HIDDEN STEP -->
                            <input type="hidden" name="step" value="4">


                            <h3 class="body-large-size">4. What thickness spray foam do you need?</h3>
                            <hr class="hr-70 ml-0 mt-0_5">
                            <div class="row">
                                <div class="col-6 col-md thickness-choice">
                                    <input type="radio" name="foam-thickness" value="100mm" id="100mm" onclick="document.getElementById('thickness_of_spray_foam').value = this.value;">
                                    <label for="100mm">
                                        <img src="<?php asset_uri('images/foam-100.svg');?>" alt="little bit of foam"><br>
                                        <span class="large">100mm</span>
                                        <br><br>Homes With Average Weather Temperatures
                                    </label>
                                </div>
                                <div class="col-6 col-md thickness-choice">
                                    <input type="radio" name="foam-thickness" value="150mm" id="150mm" onclick="document.getElementById('thickness_of_spray_foam').value = this.value;">
                                    <label for="150mm">
                                        <img src="<?php asset_uri('images/foam-150.svg');?>" alt="medium amount of foam"><br>
                                        <span class="large">150mm</span>
                                        <br><br>Homes With Colder Weather Temperates
                                    </label>
                                </div>
                                <div class="col-6 col-md thickness-choice">
                                    <input type="radio" name="foam-thickness" value="200mm" id="200mm" onclick="document.getElementById('thickness_of_spray_foam').value = this.value;">
                                    <label for="200mm">
                                        <img src="<?php asset_uri('images/foam-200.svg');?>" alt="large amount of foam"><br>
                                        <span class="large">200mm</span>
                                        <br><br>For Commercial Applications
                                    </label>
                                </div>
                            </div>

                            <div class="text-center dont-know-choice">
                                <input type="radio" name="foam-thickness" value="Dont Know" id="foam-thickness-dont-know" onclick="document.getElementById('thickness_of_spray_foam').value = this.value;">
                                <label for="foam-thickness-dont-know">I don't know</label>
                            </div>
                            <input type="hidden" value="100mm" name="thickness_of_spray_foam" id="thickness_of_spray_foam">

                        </div>

                        <div id="quote-submit" class="mt-2">
                            <div class="row">
                                <div class="col-sm-6 mb-0_5 mb-sm-0">
                                    <button type="submit" class="mf-button quote-submit w-100 text-center">Request instant quote</button>
                                </div>
                                <div class="col-sm-6">
                                    <a href="tel:08002465051" class="mf-outline-button w-100 text-center">Call us now 0800 246 5051</a>
                                </div>
                            </div>
                            <p class="body-smallest-size mt-0_5">*All quotes are subject to survey</p>
                        </div><!-- /#quote-submit -->

                    </form>
                </div><!-- /#step-4 -->

                <div id="quote-step-5" class="calc-step submit-success text-center mt-2">
                    <img src="<?php asset_uri('images/icons/tick.svg');?>" width="130px" height="130px" alt="Quote Success Tick Icon">
                    <h3 class="my-1_5">Thanks! Your quote is on it's way!</h3>
                    <p class="mb-2">Thanks for choosing Mass Foam Systems. Please check your email for your instant quote or call us directly below!</p>
                    <a href="<?php echo home_url();?>" class="mf-button m-0_5">Back to homepage</a>
                    <a href="tel:08002465051" class="mf-outline-button m-0_5">Call us now 0800 246 5051</a>
                </div>

                <div id="callback_success" class="calc-step quote-callback text-center mt-2">
                    <img src="<?php asset_uri('images/icons/tick.svg');?>" width="130px" height="130px" alt="Call back Success Tick Icon">
                    <h3 class="my-1_5">Thanks! We will call you back!</h3>
                    <a href="<?php echo home_url();?>" class="mf-button m-0_5">Back to homepage</a>
                    <a href="tel:08002465051" class="mf-outline-button m-0_5">Call us now 0800 246 5051</a>
                </div>

            </div><!-- /.gform_wrapper -->

        </div>
    </div>
</div>



