<?php

$eligible_calc = new EligibilityCalculator();

?>

<div class="container">
    <div class="page-content-wrap">
        <div class="page-content">
            <?php get_template_part('templates/page', 'title'); ?>
            <?php if (get_the_content()) : ?>
                <div class="mb-1">
                    <?php the_content(); ?>
                    <hr>
                </div>
            <?php endif; ?>

            <div class="gform_wrapper mfs-form">
                <div id="eligible-step-1" class="calc-step active mt-1">
                    <form>
                        <input type="hidden" name="step" value="1">
                        <h3 class="body-large-size">1. A bit about you</h3>
                        <hr class="hr-70 ml-0 mt-0_5">
                        <ul>
                            <li>
                                <label class="small-label">Your name<sup>*</sup></label>
                                <input name="name" type="text" placeholder="Your Name" required>
                            </li>
                            <li>
                                <label class="small-label">Your email address<sup>*</sup></label>
                                <input name="email" type="email" placeholder="Email Address" required>
                            </li>
                            <li>
                                <div class="row align-items-center">
                                    <div class="col-sm-6 col-lg-4">
                                        <label class="small-label">Your telephone number<sup>*</sup></label>
                                        <input name="phone" type="tel" placeholder="Telephone" required>
                                    </div>
                                    <div class="col-sm">
                                        <div class="form-explainer mb-1 mb-sm-0 mt-sm-1">
                                            <p><strong>Why do we need your phone number?</strong><br>
                                                Don’t worry, we <em>never</em> use call centers and will never cold call. However it’s important to talk to our customers to ensure they obtain all the information they need. By clicking submit you agree that one of our insulation specialists may get in touch with you about your submission.</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row align-items-center">
                                    <div class="col-sm-6 col-lg-4">
                                        <label class="small-label">Your postcode<sup>*</sup></label>
                                        <input name="postcode" type="text" placeholder="Postcode" required>
                                    </div>
                                    <div class="col-sm">
                                        <div class="form-explainer mb-1 mb-sm-0 mt-sm-1">
                                            <p><strong>Why do we need your postcode?</strong><br>
                                                We need your postcode so we can determine whether you are in the correct location for this grant scheme.
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="mt-0_75">
                                <input type="checkbox" name="privacy_agreement" required><label class="body-small-size">I confirm that by submitting this form I am agreeing to the <a href="/privacy-policy">Privacy Policy</a> </label>
                            </li>
                        </ul>
                        <a class="mf-button eligible-submit mt-1_5" id="step-1-submit" href="#">Next ></a>
                    </form>
                </div><!-- /#step-1 -->

                <div id="eligible-step-2" class="calc-step mt-2">
                    <form>
                        <input type="hidden" name="step" value="2">
                        <h3 class="body-large-size">2. What are you looking to insulate?</h3>
                        <hr class="hr-70 ml-0 mt-0_5">

                        <div class="row">
                            <div class="col-3 col-md radio-choice">
                                <input type="radio" name="insulation_area_radio" value="pitched_roof" id="pitched_roof_insulation" onclick="document.getElementById('insulation_area').value = this.value;">
                                <label for="pitched_roof_insulation">Pitched Roof</label>
                            </div>
                            <div class="col-3 col-md radio-choice">
                                <input type="radio" name="insulation_area_radio" value="loft" id="loft_insulation" onclick="document.getElementById('insulation_area').value = this.value;">
                                <label for="loft_insulation">Loft</label>
                            </div>
                            <div class="col-3 col-md radio-choice">
                                <input type="radio" name="insulation_area_radio" value="cavity_wall" id="cavity_wall_insulation" onclick="document.getElementById('insulation_area').value = this.value;">
                                <label for="cavity_wall_insulation">Cavity Wall</label>
                            </div>
                            <div class="col-3 col-md radio-choice">
                                <input type="radio" name="insulation_area_radio" value="other" id="other_insulation" onclick="document.getElementById('insulation_area').value = this.value;">
                                <label for="other_insulation">Other</label>
                            </div>
                        </div>

                        <input type="hidden" name="insulation_area" id="insulation_area">
                    </form>
                </div><!-- /#step-2 -->

                <div id="eligible-step-3" class="calc-step mt-2">
                    <form>
                        <input type="hidden" name="step" value="3">
                        <h3 class="body-large-size">3. Are you the named homeowner?</h3>
                        <hr class="hr-70 ml-0 mt-0_5">

                        <p>Select 'Yes' if you can answer 'Yes' to any of these questions:</p>
                        <ul>
                            <i>
                                <li>Do you own your home (including long-leaseholders and shared ownership)?</li>
                                <li>Do you own your park home on a residential site (including Gypsy and Traveller sites)?</li>
                                <li>Are you a residential landlord in the private or social rented sector (inluding local authorites and housing associations</li>
                            </i>
                        </ul>

                        <div class="form-explainer my-1_5">
                            <p><strong>Why?</strong><br>
                                This is a key question that resolves in initial eligibility for the Government Green Homes Grant.
                        </div>

                        <div class="row">
                            <div class="col-6 col-md radio-choice">
                                <input type="radio" name="ownership_radio" value="yes" id="yes-ownership" onclick="document.getElementById('ownership').value = this.value;">
                                <label for="yes-ownership">Yes</label>
                            </div>
                            <div class="col-6 col-md radio-choice">
                                <input type="radio" name="ownership_radio" value="no" id="no-ownership" onclick="document.getElementById('ownership').value = this.value;">
                                <label for="no-ownership">No</label>
                            </div>
                        </div>

                        <input type="hidden" name="ownership" id="ownership">
                    </form>
                </div><!-- /#step-3 -->

                <div id="eligible-step-4" class="calc-step mt-2">
                    <form>
                        <input type="hidden" name="step" value="4">
                        <h3 class="body-large-size">4. Do you claim benefits?</h3>
                        <hr class="hr-70 ml-0 mt-0_5">

                        <p>Select 'Yes' if you receive any of the following benefits:</p>

                        <ul>
                            <i>
                                <li>Income Support</li>
                                <li>Income-Based Jobseeker’s Allowance (JSA)</li>
                                <li>Income-Based Employment and Support Allowance (ESA)</li>
                                <li>Universal Credit</li>
                                <li>Pension Guarantee Credit (excluding Pension Savings Credit)</li>
                                <li>Child Tax Credit</li>
                                <li>Working Tax Credit</li>
                                <li>Disability Living Allowance</li>
                                <li>Personal Independence Payment (PIP)</li>
                                <li>Attendance Allowance</li>
                                <li>Carer's Allowance</li>
                                <li>Severe Disablement Allowance</li>
                                <li>Industrial Injuries Disablement Benefits</li>
                                <li>Contribution-Based Jobseeker’s Allowance (JSA)</li>
                                <li>Contribution-Based Employment and Support Allowance (ESA)</li>
                                <li>Housing Benefit</li>
                            </i>
                        </ul>

                        <div class="form-explainer my-1_5">
                            <p><strong>Why?</strong><br>
                                We understand this may be personal, however this key information to help determine how much funding you can be provided with.
                        </div>

                        <div class="row">
                            <div class="col-6 col-md radio-choice">
                                <input type="radio" name="benefits_radio" value="yes" id="yes-benefits" onclick="document.getElementById('benefits').value = this.value;">
                                <label for="yes-benefits">Yes</label>
                            </div>
                            <div class="col-6 col-md radio-choice">
                                <input type="radio" name="benefits_radio" value="no" id="no-benefits" onclick="document.getElementById('benefits').value = this.value;">
                                <label for="no-benefits">No</label>
                            </div>
                        </div>
                        <input type="hidden" name="benefits" id="benefits">
                    </form>
                </div><!-- /#step-4 -->

                <!-- Messages START -->
                <div id="not-applicable-msg" class="mt-2 calc-grant" style="display: none;">
                    <h3 class="body-large-size">Unfortunately, you are not applicable</h3>
                    <hr class="hr-70 ml-0 mt-0_5">

                    <p>Unfortunately, we cannot determine if you are applicable for this grant scheme.</p>
                </div>

                <div id="five-k-grant" class="mt-2 calc-grant" style="display: none;">
                    <h3 class="body-large-size">Congratulations!</h3>
                    <hr class="hr-70 ml-0 mt-0_5">

                    <p>You are eligible for up to £5000 to help go towards your insulation.</p>
                    <p>Be aware that you will need to pay 1/3 of the total insulation quotation.</p>
                </div>

                <div id="ten-k-grant" class="mt-2 calc-grant" style="display: none;">
                    <h3 class="body-large-size">Congratulations!</h3>
                    <hr class="hr-70 ml-0 mt-0_5">

                    <p>You are eligible for up to £10000 to help go towards your insulation.</p>
                    <p>You also have the benefit that your insulation quotation will be 100% covered by this grant.</p>
                </div>
                <!-- Messages END -->

                <div id="submit-step" class="calc-step mt-2" style="display: none;">
                    <form>
                        <input type="hidden" name="step" value="5">
                        <button type="submit" class="mf-button w-100 text-center eligible-submit">Submit and get started</button>
                        <p class="body-smallest-size text-center mt-0_5">*All quotes are subject to survey</p>
                    </form>
                </div><!-- /#form-submit -->

                <div id="eligible-step-6" class="calc-step submit-success text-center mt-2">
                    <img src="<?php asset_uri('images/icons/tick.svg'); ?>" width="130px" height="130px" alt="Eligible Success Tick Icon">
                    <h3 class="my-1_5">Thanks! Your submission is on it's way!</h3>
                    <p class="mb-2">Thanks for choosing Mass Foam Systems. Please check your email for your submission details or call us directly below!</p>
                    <a href="<?php echo home_url(); ?>" class="mf-button m-0_5">Back to homepage</a>
                    <a href="tel:08002465051" class="mf-outline-button m-0_5">Call us now 0800 246 5051</a>
                </div><!-- /#step-6 -->
            </div>
        </div>
    </div>
</div>