<?php

$eco_calc = new EcoCalculator();

?>

<div class="container">
    <div class="page-content-wrap">
        <div class="page-content">
            <?php get_template_part('templates/page', 'title'); ?>
            <?php if (get_the_content()) : ?>
                <div class="mb-1">
                    <?php the_content(); ?>
                    <hr>
                </div>
            <?php endif; ?>

            <div class="gform_wrapper mfs-form">
                <div id="eco-step-1" class="calc-step active mt-1">
                    <form>
                        <input type="hidden" name="step" value="1">
                        <h3 class="body-large-size">1. A bit about you</h3>
                        <hr class="hr-70 ml-0 mt-0_5">
                        <ul>
                            <li>
                                <label class="small-label">Your name<sup>*</sup></label>
                                <input name="name" type="text" placeholder="Your Name" required>
                            </li>
                            <li>
                                <label class="small-label">Your email address<sup>*</sup></label>
                                <input name="email" type="email" placeholder="Email Address" required>
                            </li>
                            <li>
                                <div class="row align-items-center">
                                    <div class="col-sm-6 col-lg-4">
                                        <label class="small-label">Your telephone number<sup>*</sup></label>
                                        <input name="phone" type="tel" placeholder="Telephone" required>
                                    </div>
                                    <div class="col-sm">
                                        <div class="form-explainer mb-1 mb-sm-0 mt-sm-1">
                                            <p><strong>Why do we need your phone number?</strong><br>
                                                Don't worry, we <em>never</em> use call centers and will never cold call. However it's important to talk to our customers to ensure they obtain all the information they need. By clicking submit you agree that one of our insulation specialists may get in touch with you about your submission.</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row align-items-center">
                                    <div class="col-sm-6 col-lg-4">
                                        <label class="small-label">Your postcode<sup>*</sup></label>
                                        <input name="postcode" type="text" placeholder="Postcode" required>
                                    </div>
                                    <div class="col-sm">
                                        <div class="form-explainer mb-1 mb-sm-0 mt-sm-1">
                                            <p><strong>Why do we need your postcode?</strong><br>
                                                We need your postcode so we can determine whether you are in the correct location for this scheme.
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="mt-0_75">
                                <input type="checkbox" name="privacy_agreement" required><label class="body-small-size">I confirm that by submitting this form I am agreeing to the <a href="/privacy-policy">Privacy Policy</a> </label>
                            </li>
                        </ul>
                        <a class="mf-button eco-submit mt-1_5" id="step-1-submit" href="#">Next ></a>
                    </form>
                </div><!-- /#step-1 -->

                <div id="eco-step-2" class="calc-step mt-2">
                    <form>
                        <input type="hidden" name="step" value="2">
                        <h3 class="body-large-size">2. Homeowner permission?</h3>
                        <hr class="hr-70 ml-0 mt-0_5">

                        <p>Select 'Yes' if you can answer 'Yes' to any of these questions:</p>
                        <ul>
                            <i>
                                <li>Do you own your home (including long-leaseholders and shared ownership)?</li>
                                <li>Can you obtain the homeowner's permission with regards to home improvements?</li>
                            </i>
                        </ul>

                        <div class="form-explainer my-1_5">
                            <p><strong>Why?</strong><br>
                                This is a key question that resolves in initial eligibility for the ECO4 scheme.
                        </div>

                        <div class="row">
                            <div class="col-6 col-md radio-choice">
                                <input type="radio" name="permission_radio" value="yes" id="yes-permission" onclick="document.getElementById('permission').value = this.value;">
                                <label for="yes-permission">Yes</label>
                            </div>
                            <div class="col-6 col-md radio-choice">
                                <input type="radio" name="permission_radio" value="no" id="no-permission" onclick="document.getElementById('permission').value = this.value;">
                                <label for="no-permission">No</label>
                            </div>
                        </div>

                        <input type="hidden" name="permission" id="permission">
                    </form>
                </div><!-- /#step-2 -->

                <div id="eco-step-3" class="mt-2 calc-step">
                    <form>
                        <input type="hidden" name="step" value="3">
                        <h3 class="body-large-size">3. What is your EPC rating?</h3>
                        <hr class="hr-70 ml-0 mt-0_5">

                        <p>What is the EPC rating of your home</p>

                        <div class="row">
                            <div class="col-12 col-md radio-choice">
                                <input type="radio" name="epc_rating_radio" value="A" id="epc-a" onclick="document.getElementById('epc-rating').value = this.value;">
                                <label for="epc-a">A</label>
                            </div>
                            <div class="col-12 col-md radio-choice">
                                <input type="radio" name="epc_rating_radio" value="B" id="epc-b" onclick="document.getElementById('epc-rating').value = this.value;">
                                <label for="epc-b">B</label>
                            </div>
                            <div class="col-12 col-md radio-choice">
                                <input type="radio" name="epc_rating_radio" value="C" id="epc-c" onclick="document.getElementById('epc-rating').value = this.value;">
                                <label for="epc-c">C</label>
                            </div>
                            <div class="col-12 col-md radio-choice">
                                <input type="radio" name="epc_rating_radio" value="D" id="epc-d" onclick="document.getElementById('epc-rating').value = this.value;">
                                <label for="epc-d">D</label>
                            </div>
                            <div class="col-12 col-md radio-choice">
                                <input type="radio" name="epc_rating_radio" value="E" id="epc-e" onclick="document.getElementById('epc-rating').value = this.value;">
                                <label for="epc-e">E</label>
                            </div>
                            <div class="col-12 col-md radio-choice">
                                <input type="radio" name="epc_rating_radio" value="F" id="epc-f" onclick="document.getElementById('epc-rating').value = this.value;">
                                <label for="epc-f">F</label>
                            </div>
                            <div class="col-12 col-md radio-choice">
                                <input type="radio" name="epc_rating_radio" value="G" id="epc-g" onclick="document.getElementById('epc-rating').value = this.value;">
                                <label for="epc-g">G</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md radio-choice">
                                <input type="radio" name="epc_rating_radio" value="Unknown" id="epc-unknown" onclick="document.getElementById('epc-rating').value = this.value;">
                                <label for="epc-unknown">Unknown</label>
                            </div>
                        </div>

                        <input type="hidden" name="epc-rating" id="epc-rating">
                    </form>
                </div><!-- /#step-3 -->

                <div id="eco-step-4" class="calc-step mt-2">
                    <form>
                        <input type="hidden" name="step" value="4">
                        <h3 class="body-large-size">4. Do you receive benefits?</h3>
                        <hr class="hr-70 ml-0 mt-0_5">

                        <p>Select 'Yes' if you receive any of the following benefits:</p>

                        <ul>
                            <i>
                                <li>Income-based Jobseeker's Allowance (JSA)</li>
                                <li>Income-related Employment and Support Allowance (ESA)</li>
                                <li>Income Support (IS)</li>
                                <li>Pension Guarantee Credit</li>
                                <li>Working Tax Credit (WTC)</li>
                                <li>Child Tax Credit (CTC)</li>
                                <li>Universal Credit (UC)</li>
                                <li>Housing Benefit</li>
                                <li>Pension Credit Savings</li>
                            </i>
                        </ul>

                        <div class="form-explainer my-1_5">
                            <p><strong>Why?</strong><br>
                                We understand this may be personal, however this key information to help determine if you are applicable for the ECO4 scheme.
                        </div>

                        <div class="row">
                            <div class="col-6 col-md radio-choice">
                                <input type="radio" name="benefits_radio" value="yes" id="yes-benefits" onclick="document.getElementById('benefits').value = this.value;">
                                <label for="yes-benefits">Yes</label>
                            </div>
                            <div class="col-6 col-md radio-choice">
                                <input type="radio" name="benefits_radio" value="no" id="no-benefits" onclick="document.getElementById('benefits').value = this.value;">
                                <label for="no-benefits">No</label>
                            </div>
                        </div>
                        <input type="hidden" name="benefits" id="benefits">
                    </form>
                </div><!-- /#step-4 -->

                <div id="eco-step-5" class="calc-step mt-2">
                    <form>
                        <input type="hidden" name="step" value="5">
                        <h3 class="body-large-size">5. Other Conditions?</h3>
                        <hr class="hr-70 ml-0 mt-0_5">

                        <p>Select 'Yes' if any of the following is true:</p>

                        <!-- TODO: Needs rewording -->

                        <ul>
                            <i>
                                <li>You have been referred to this scheme by your energy supplier</li>
                                <li>Your home is identified as a "Cold Home" by the National Institute for Health and Care Excellence (NICE)</li>
                                <li>You have been referred to this scheme by the NHS:</li>
                                <ul class="ml-1">
                                    <li>By a general medical practitioner provider</li>
                                    <li>By an NHS Foundation Trust</li>
                                    <li>By an NHS Trust</li>
                                    <li>By a health board (inc. local health boards)</li>
                                </ul>
                            </i>
                        </ul>

                        <div class="form-explainer my-1_5">
                            <p><strong>Why?</strong><br>
                            By answering this question, you may be applicable for the ECO4 Flex Proxy Targeting route.
                        </div>

                        <div class="row">
                            <div class="col-6 col-md radio-choice">
                                <input type="radio" name="referral_radio" value="yes" id="yes-referral" onclick="document.getElementById('referral').value = this.value;">
                                <label for="yes-referral">Yes</label>
                            </div>
                            <div class="col-6 col-md radio-choice">
                                <input type="radio" name="referral_radio" value="no" id="no-referral" onclick="document.getElementById('referral').value = this.value;">
                                <label for="no-referral">No</label>
                            </div>
                        </div>
                        <input type="hidden" name="referral" id="referral">
                    </form>
                </div><!-- /#step-5 -->

                <!-- Messages START -->
                <div id="not-applicable-msg" class="mt-2 calc-grant" style="display: none;">
                    <h3 class="body-large-size">Unfortunately, you are not applicable</h3>
                    <hr class="hr-70 ml-0 mt-0_5">

                    <p>Unfortunately, we cannot determine if you are applicable for the ECO4 scheme.</p>

                    <a href="/request-a-call-back" class="mf-button w-100 text-center">Request a call back</a>
                </div>

                <div id="eco-applicable" class="mt-2 calc-grant" style="display: none;">
                    <h3 class="body-large-size">Congratulations!</h3>
                    <hr class="hr-70 ml-0 mt-0_5">

                    <p>Based on the information you have provided, you are eligible for the ECO4 scheme.</p>
                </div>
                <!-- Messages END -->

                <div id="submit-step" class="calc-step mt-1" style="display: none;">
                    <form>
                        <input type="hidden" name="step" value="6">
                        <button type="submit" class="mf-button w-100 text-center eco-submit">Submit and get started</button>
                    </form>
                </div><!-- /#form-submit -->

                <div id="eco-step-7" class="calc-step submit-success text-center mt-2">
                    <img src="<?php asset_uri('images/icons/tick.svg'); ?>" width="130px" height="130px" alt="Eligible Success Tick Icon">
                    <h3 class="my-1_5">Thanks! Your submission is on it's way!</h3>
                    <p class="mb-2">Thanks for choosing Mass Foam Systems. Please check your email for your submission details or call us directly below!</p>
                    <a href="<?php echo home_url(); ?>" class="mf-button m-0_5">Back to homepage</a>
                    <a href="tel:08002465051" class="mf-outline-button m-0_5">Call us now 0800 246 5051</a>
                </div><!-- /#step-6 -->
            </div>
        </div>
    </div>
</div>