<div class="container">
    <div class="page-content-wrap">
        <div class="page-content">
            <?php get_template_part('templates/page', 'title'); ?>
            <?php the_content(); ?>
        </div>
    </div>
</div>



