<?php
    $expanded_content = get_field('expanded_page_content');
    if($expanded_content):
?>

    <div class="expanded-content">
        <div class="expanded-content-inner">
            <?php echo $expanded_content;?>
        </div>
        <button class="js-expand-read-more mf-outline-button">Read More</button>
    </div>

<?php endif;?>
