<?php
    $steps = get_field('process_steps');
    $cta_text = get_field('process_bottom_cta_text');
    $cta_link = get_field('process_bottom_cta_link');
?>

<div class="container">
    <div class="page-content-wrap">
        <div class="page-content">
            <?php get_template_part('templates/page', 'title'); ?>
            <?php the_content(); ?>

            <?php if($steps):?>
                <div class="process-step-wrapper">

                    <?php foreach ($steps as $step_i => $step_k):
                        $number = $step_i + 1;
                        $title = $step_k['step_title'];
                        $icon = $step_k['step_icon'];
                        $text = $step_k['step_content'];
                    ?>

                    <?php if($number == 1) { ?>

                        <div class="first-step py-1_5 px-1_5 px-md-2_5 text-center text-sm-left">
                            <span class="step-number white-step"><?php echo $title;?></span>
                            <p class="step-1-text"><?php echo $text;?></p>

                            <a class="mf-button white-button mx-1 ml-sm-0 mb-0_5 mb-sm-0" href="tel:08002465051">Call us now</a>
                            <a class="mf-button white-outline" href="<?php echo get_permalink( get_page_by_path( 'request-a-call-back' ) );?>">Request a call back</a>
                            <img src="<?php echo $icon['url'];?>" class="step-1-image" alt="<?php echo $icon['alt'];?>">
                        </div>

                    <?php } else { ?>
                        <div class="step-holder step-<?php echo $number;?>">
                            <?php if($step_i % 2 == 0) {?>

                                <svg width="722" height="77" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="101 122 722 77">
                                    <path stroke-width="2px" stroke="#009688" stroke-linecap="round" stroke-linejoin="round" d="M102 123v34c0 2.8 2.2 5 5 5h710c2.8 0 5 2.2 5 5v31"/>
                                    <path stroke-width="2px" stroke="#009688" stroke-linecap="round" stroke-linejoin="round" d="M449.8 179.7l18-18-18-18"/>
                                </svg>

                            <?php } else { ?>

                                <svg width="722" height="77" fill="none" xmlns="http://www.w3.org/2000/svg" viewbox="101 122 722 77">
                                    <path d="M822 123v34c0 2.8-2.2 5-5 5H107c-2.8 0-5 2.2-5 5v31" stroke-width="2px" stroke="#009688" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M446 144.1l-18 18 18 18" stroke-width="2px" stroke="#009688" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>

                            <?php } ?>


                            <div class="step-box d-flex align-items-center <?php if($step_i % 2 == 0) echo ' flex-md-row-reverse';?>">
                                <img src="<?php echo $icon['url'];?>" class="step-icon mx-0_5" alt="<?php echo $icon['alt'];?>">
                                <div class="step-content p-1 pt-md-0">
                                    <span class="step-number"><?php echo $title ;?></span>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <p class="step-text mb-md-0"><?php echo $text;?></p>
                                        </div>

                                        <div class="col d-md-none">
                                            <a class="mf-button step-trigger p-0_5" href="#" data-target="step-<?php echo $number + 1;?>"><img src="<?php asset_uri('images/icons/arrow-down.svg');?>" width="16" alt="arrow pointing downwards"></a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    <?php } endforeach;?>

                </div>
            <?php endif;?>


            <?php if($cta_text): ?>
                <div class="get_quote_cta step-7 mt-2">
                    <p><?php echo $cta_text;?></p>
                    <a class="mf-button white-button" href="<?php echo $cta_link['url'];?>"><?php echo $cta_link['title'];?></a>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>



