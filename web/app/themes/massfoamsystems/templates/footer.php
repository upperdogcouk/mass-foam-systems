<?php
    $footer_blurb = get_field('footer_about_blurb','option');
    $downloads = get_field('documents_&_downloads','option');
    $phone = get_field('contact_freephone_number','option');
    $poole = get_field('poole_address','option');
    $essex = get_field('essex_address','option');
    $poole_phone = get_field('poole_phone','option');
    $essex_phone = get_field('essex_phone','option');
?>

<footer class="main-footer pt-1_5 pt-md-2_5 pt-lg_3 pb-1_5">
    <div class="container">
        <div class="row">
            <div class="col pb-1_5">
                <h4>Finance</h4>
                <a href="https://ideal4finance.com/MFS?utm_source=3rd&utm_medium=banner&utm_campaign=Banners" target="_blank" rel="noopener noreferrer" title="Ideal4Finance" class="finance-widget"><img data-src="<?php asset_uri('images/i4f-banner.gif');?>" class="img-fluid lozad" width="468px" alt="Ideal4Finance"/></a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-lg-3 mb-1_5 mb-lg-0">
                <h4>About</h4>
                <p class="body-smallest-size mb-0_5 pr-sm-0_5"><?php echo $footer_blurb;?></p>

                <div class="row no-gutters align-items-center mt-0_75">
                    <div class="col-auto">
                        <img class="lozad" data-src="<?php asset_uri('images/channel-4-logo.svg');?>" alt="Channel 4 logo">
                    </div>
                    <div class="col pl-0_5">
                        <p class="mb-0 body-smallest-size ff-mont">Featured on Channel 4's Grand Designs</p>
                    </div>
                </div>

                <img data-src="<?php asset_uri('images/footer-accreditations.png');?>" alt="Accrediations" width="221px" class="img-fluid footer-accreditations mt-0_75 lozad">

            </div>

            <div class="col-sm-6 col-lg-3 mb-1_5 mb-lg-0">
                <h4>Quick links</h4>
                <?php wp_nav_menu(
                    array(
                      'menu' => 'Footer',
                      'menu_class' => 'footer-menu',
                      'walker' => FALSE
                    )
                  );
                ?>
            </div>

            <div class="col-sm-6 col-lg-3 mb-1_5 mb-sm-0">
                <h4>Documents &amp; Downloads</h4>
                <?php if($downloads):?>
                <ul class="footer-menu">
                    <?php foreach($downloads as $download):?>
                        <li><a href="<?php echo $download['file'];?>" target="_blank" title="<?php echo $download['name'];?>"><?php echo $download['name'];?></a></li>
                    <?php endforeach;?>
                </ul>
                <?php endif; ?>
            </div>

            <div class="col-sm-6 col-lg-3">
                <h4>Contact</h4>

                <div class="row">
                    <div class="col-6 col-lg-12 col-xl-6">
                        <p class="body-small-size"><strong>Free Phone</strong><br>
                            <a href="tel:<?php echo $phone;?>"><?php echo $phone;?></a></p>
                    </div>
                    <div class="col-6 col-lg-12 col-xl-6 socials">
                        <a href="https://www.facebook.com/massfoamsystemsltd/" rel="noopener noreferrer" target="_blank" title="Follow us on Facebook"><img width="36px" class="mb-0_5 mr-0_5" src="<?php asset_uri('images/icons/facebook-circle.svg');?>" alt="Facebook Icon"></a>
                        <a href="https://twitter.com/massfoamsystems" target="_blank" rel="noopener noreferrer" title="Follow us on Twitter"><img width="36px" class="mb-0_5 mr-0_5" src="<?php asset_uri('images/icons/twitter-circle.svg');?>" alt="Twitter Icon"></a>
                        <a href="https://www.instagram.com/massfoamsystems/" target="_blank" rel="noopener noreferrer" title="Follow us on Instagram"><img width="36" class="mb-0_5 mr-0_5" src="<?php asset_uri('images/icons/instagram-circle.svg');?>" alt="Instagram Icon"></a>
                    </div>
                    <div class="col-6 col-lg-12 col-xl-6">
                        <p class="body-small-size">
                            <strong>Essex Office</strong><br>
                            <?php echo $essex;?>
                        </p>

                        <p class="body-small-size"><a href="tel:<?php echo $poole_phone;?>"><?php echo $poole_phone;?></a></p>
                    </div>
                    <div class="col-6 col-lg-12 col-xl-6">
                        <p class="body-small-size">
                            <strong>Poole Office</strong><br>
                            <?php echo $poole;?>
                        </p>
                        <p class="body-small-size"><a href="tel:<?php echo $essex_phone;?>"><?php echo $essex_phone;?></a></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row copyright mt-1 mt-md-2 pb-3 pb-md-0">
            <div class="col-sm mb-0_5 mb-sm-0">
                <p class="mb-0 body-smallest-size">
                    Copyright <?php echo date('Y');?> Mass Foam Systems Ltd | No.1 UK HBS Spray Foam Insulation Installers | <a href="<?php echo get_permalink('149');?>">Terms & Conditions</a> | <a href="<?php echo get_permalink('147');?>">Privacy Policy</a>
                </p>
            </div>
            <div class="col-sm-auto">
                <p class="mb-0 body-smallest-size">
                    <a rel="noopener noreferrer" target="_blank" href="https://www.webspred.com" title="Crafted by Webspred">Crafted by Webspred</strong></a>
                </p>
            </div>
        </div>
    </div>
</footer>

<div class="mobile-footer-cta">
    <div class="row no-gutters">
        <a class="col-6" href="tel:<?php echo $phone;?>">
            <img src="<?php asset_uri('images/icons/phone-icon-white.svg');?>" alt="phone icon"><br>
            <span class="ff-mont body-smallest-size">Call us</span>
        </a>
        <a class="col-6" href="<?php echo get_permalink(39);?>">
            <img src="<?php asset_uri('images/icons/mail-icon-white.svg');?>" alt="mail icon"><br>
            <span class="ff-mont body-smallest-size">Get quote</span>
        </a>
    </div>
</div>