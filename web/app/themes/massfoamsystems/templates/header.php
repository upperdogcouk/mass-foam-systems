<?php $phone = get_field('contact_freephone_number', 'option'); ?>
<?php $trustpilot_score = get_field('trustpilot_score', 'option'); ?>

<!-- <?php
        $httpClient = new \GuzzleHttp\Client();
        $response = $httpClient->get('https://www.trustpilot.com/review/www.massfoamsystems.co.uk');
        $htmlString = (string) $response->getBody();
        // this line is to suppress any warnings
        libxml_use_internal_errors(true);
        $doc = new DOMDocument();
        $doc->loadHTML($htmlString);
        $xpath = new DOMXPath($doc);
        $titles = $xpath->evaluate('//p[@data-rating-typography="true"]');
        $firstElem = reset($titles);

        $extractedTitles = [];
        foreach ($titles as $title) {
            $trustpilot_score = $title->textContent . PHP_EOL;
            break;
        }
        ?> -->

<div class="sticky-cta-top">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-md-auto mb-0_5 mb-md-0 sticky-cta-top-quote">
                <a href="<?php echo get_permalink(get_page_by_path('request-a-call-back')); ?>" class="d-block" title="Request a callback for a quote">We will beat any like for like quote <img aria-hidden="true" src="<?php asset_uri('images/icons/arrow-right.svg'); ?>" width="8px" class="ml-0_5" alt="right arrow"><img aria-hidden="true" src="<?php asset_uri('images/icons/arrow-right.svg'); ?>" width="8px" alt="right arrow"></a>
            </div>
            <div class="col-md-auto">
                <a href="https://www.trustmark.org.uk/firms/Mass-Foam-Systems-Ltd-2982930/Upton-40614?fromSearch=true&distance=6.8%20miles&matchedTrades=75" class="d-flex align-items-center" title="TrustMark" target="_blank">Government Endorsed Quality</a>
            </div>
        </div>
    </div>
</div>

<header class="main-header">
    <div class="container">
        <div class="row no-gutters justify-content-between align-items-center">
            <div class="col-auto col-sm-auto logo-holder">
                <a href="<?php echo home_url(); ?>" title="Home">
                    <img src="<?php asset_uri('images/mass-foam-systems-logo.png'); ?>" alt="mass foam systems logo, insulation contractors logo">
                </a>
            </div>
            <div class="col-auto col-sm text-right d-flex justify-content-end align-items-center">

                <!-- TrustBox widget - Micro Star -->
                <!-- <div class="d-md-inline d-none trustpilot-widget" data-locale="en-GB" data-template-id="5419b732fbfb950b10de65e5" data-businessunit-id="5ad8867d40ff7f0001ef011b" data-style-height="24px" data-style-width="100%" data-theme="light">
                    <a href="https://uk.trustpilot.com/review/www.massfoamsystems.co.uk" target="_blank" rel="noopener">Trustpilot</a>
                </div> -->
                <!-- End TrustBox widget -->

                <?php if ($trustpilot_score && $trustpilot_score >= 3.8 && $trustpilot_score <= 5) : ?>
                    <a class="d-none d-lg-inline strong" href="https://www.trustpilot.com/review/www.massfoamsystems.co.uk" rel="noopener noreferrer" target="_blank">
                        <span class="align-middle"><?php echo $trustpilot_score ?></span>
                        <img class="trustpilot-score" src="<?php asset_uri('images/stars/' . str_replace(".", "_", strval(round($trustpilot_score / .5) * .5)) . 'stars.svg') ?>" alt="TrustPilot score">
                    </a>
                <?php endif; ?>

                <div class="d-none d-md-inline strong mr-md-1_75 ml-lg-1_75 align-middle">
                    <div class="d-none d-md-flex text-left flex-column cta-phone">
                        <span>Need Help Now? Call Us!</span>
                        <a href="tel:<?php echo $phone; ?>"><img class="mr-0_25" src="<?php asset_uri('images/icons/phone-icon-green.svg'); ?>" alt="Call us"><?php echo $phone; ?></a>
                    </div>
                </div>

                <a class="mf-button d-md-none p-0_5" href="tel:<?php echo $phone; ?>"><img src="<?php asset_uri('images/icons/phone-icon-white.svg'); ?>" alt="Call us"></a>

                <a class="mf-button d-none d-sm-inline-block ml-md-0 ml-sm-0_5" href="<?php echo get_permalink(get_page_by_path('request-a-call-back')); ?>">Request a call back</a>

                <?php if (!is_page_template('template-get-quote.php')) { ?>
                    <img src="<?php asset_uri('images/icons/nav-hamburger.svg'); ?>" id="nav-toggle-open" class="ml-0_5" alt="hamburger menu">
                    <img src="<?php asset_uri('images/icons/nav-close.svg'); ?>" id="nav-toggle-close" class="ml-0_5" alt="close menu icon">
                <?php } ?>
            </div>
        </div>
    </div>
</header>

<?php if (is_page_template('template-get-quote.php')) { ?>
    <div class="get-quote-nav-banner">
        Get an instant, no obligation quote, it takes less than 2 minutes!
    </div>
<?php } else { ?>

    <div class="navigation-wrapper">
        <nav class="desktop-nav-wrapper">
            <div class="container">
                <?php wp_nav_menu(
                    array(
                        'menu' => 'Main',
                        'menu_class' => 'main-nav',
                        'walker' => FALSE
                    )
                );
                ?>
            </div>
        </nav>
        <nav class="mobile-nav-wrapper">

            <?php wp_nav_menu(
                array(
                    'menu' => 'Main',
                    'menu_class' => 'mobile-nav',
                    'walker' => FALSE
                )
            );
            ?>

        </nav>
    </div>

<?php } ?>