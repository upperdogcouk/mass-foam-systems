<?php
    $tab_item = get_field('tab_item');
    $why_list = get_field('why_list');
    $extra_section_header = get_field('extra_section_header');
    $extra_section_text = get_field('extra_section_text');
    $open_cell = get_field('open_cell');
    $closed_cell = get_field('closed_cell');
?>
    <div class="container">
        <div class="page-content-wrap mb-0">
            <div class="page-content">
                <?php get_template_part('templates/page', 'title'); ?>
                <?php the_content(); ?>
                <?php get_template_part('templates/content', 'expanded'); ?>
            </div>
        </div>

        <div class="page-content-wrap tab-nav mb-0 mt-0">
            <div class="page-content">
                <h3 class="section-title text-center">4 Reasons To Use Icynene</h3>
                <hr class="hr-70">
                <ul class="nav nav-tabs" id="nav-tab" role="tablist">
                    <div class="magic-box"></div>
                    <?php foreach ($tab_item as $tab_i => $tab_k):
                        $tab_title = $tab_k['tab_title'];
                        $tab_id = $tab_k['tab_id']; ?>
                        <li class="tab col-3 text-center p-1 d-flex align-items-center justify-content-center">
                            <a data-toggle="tab" href="#<?php echo $tab_id; ?>"
                               class="text-center <?php if ($tab_i == 0) echo 'active'; ?>">
                                <span class="tab-num"><?php echo $tab_i + 1; ?></span>
                                <span class="tab-hide"> <?php echo $tab_title; ?></span></a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
        <div class="page-content-wrap tab-body mb-0 mt-0">
            <div class="page-content">
                <div class="tab-content" id="nav-tab" role="tablist">
                    <?php foreach ($tab_item as $tab_i => $tab_k):
                        $tab_title = $tab_k['tab_title'];
                        $tab_heading = $tab_k['tab_heading'];
                        $tab_text = $tab_k['tab_text'];
                        $tab_video = $tab_k['tab_video'];
                        $tab_id = $tab_k['tab_id']; ?>
                        <div id="<?php echo $tab_id; ?>" class="tab-pane <?php if ($tab_i == 0) echo 'active'; ?> "
                             role="tabpanel">
                            <h3><?php echo $tab_heading; ?> </h3>
                            <div class="row justify-content-between">
                                <div class="col-lg-8">
                                    <?php echo $tab_text; ?>
                                </div>
                                <div class="col-lg-4">
                                    <div class="video-wrapper">
                                        <?php echo $tab_video; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>

        <?php if($why_list):?>
        <div class="page-content-wrap mt-0 mb-0">
            <div class="page-content">
                <h3 class="section-title text-center">Why you need Icynene&reg; Spray Foam Insulation</h3>
                <hr class="hr-70">

                <ul class="service-list list-4cols">
                    <?php foreach($why_list as $why_k):
                        $reason = $why_k['why_list_item'];
                    ?>
                        <li><?php echo $reason;?></li>
                    <?php endforeach;?>
                </ul>

            </div>
        </div>
        <?php endif?>
    </div>

<?php
    if ($open_cell || $closed_cell):
?>
    <section class="types-of-icynene mt-1 mb-2 mb-md-3 py-1_5 py-md-3">
        <div class="container">

            <?php if($extra_section_header):?>
                <div class="text-center mb-2">
                    <h3 class="section-title"><?php echo $extra_section_header; ?></h3>
                </div>
            <?php endif;?>
            <div class="row">
                <?php if ($open_cell):
                    $open_cell_description = $open_cell['open_cell_description'];
                    $open_cell_image = $open_cell['open_cell_image'];
                    $open_cell_applications = $open_cell['open_cell_applications'];
                    $open_cell_feature_list = $open_cell['open_cell_feature_list'];
                    $open_cell_download_link = $open_cell['open_cell_download_link'];
                ?>
                    <div class="col-md-6 my-1 my-md-0">
                        <div class="benefit-card d-flex h-100 flex-column text-center home">
                            <div class="benefit-title p-1">
                                <h4 class="m-0 colour-sea-green">Icynene Open Cell Spray Foam</h4>
                            </div>
                            <div class="benefit-list p-1">
                                <div class="cell-image lozad mb-1" data-background-image="<?php echo $open_cell_image['sizes']['medium']; ?>"></div>
                                <p><?php echo $open_cell_description; ?></p>
                                <p><b>Used on:</b></p>
                                <div class="pb-1">
                                    <?php foreach ($open_cell_applications as $open_cell_application):
                                        $open_cell_application_item = $open_cell_application['open_cell_application_item'];
                                    ?>
                                        <span class="application-item"><?php echo $open_cell_application_item; ?></span>
                                    <?php endforeach; ?>
                                </div>

                                <p><b>Other Benefits:</b></p>

                                <ul class="home-benefits service-list">
                                    <?php foreach ($open_cell_feature_list as $open_cell_feature_lists):
                                        $open_cell_feature_list_item = $open_cell_feature_lists['open_cell_feature_list_item']; ?>
                                        <li><?php echo $open_cell_feature_list_item ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                            <div class="benefit-link mt-auto p-1">
                                <a class="mf-button" href="<?php echo $open_cell_download_link['url']; ?>">Download Info Pack</a>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if ($closed_cell):
                    $closed_cell_description = $closed_cell['closed_cell_description'];
                    $closed_cell_image = $closed_cell['closed_cell_image'];
                    $closed_cell_applications = $closed_cell['closed_cell_applications'];
                    $closed_cell_feature_list = $closed_cell['closed_cell_feature_list'];
                    $closed_cell_download_link = $closed_cell['closed_cell_download_link'];
                ?>
                    <div class="col-md-6 my-1 my-md-0">
                        <div class="benefit-card d-flex h-100 flex-column text-center home">
                            <div class="benefit-title p-1">
                                <h4 class="m-0 colour-sea-green">Icynene Closed Cell Spray Foam</h4>
                            </div>
                            <div class="benefit-list p-1">
                                <div class="cell-image mb-1 lozad" style="background-image: url('<?php echo $closed_cell_image['sizes']['medium']; ?>');"> </div>

                                <p><?php echo $closed_cell_description; ?></p>
                                <p><b>Used on:</b></p>

                                <div class="pb-1">
                                    <?php foreach ($closed_cell_applications as $closed_cell_application):
                                        $closed_cell_application_item = $closed_cell_application['closed_cell_application_item'];
                                    ?>
                                        <span class="application-item"><?php echo $closed_cell_application_item; ?></span>
                                    <?php endforeach; ?>
                                </div>

                                <p><b>Other Benefits:</b></p>

                                <ul class="home-benefits service-list">
                                    <?php foreach ($closed_cell_feature_list as $closed_cell_feature_lists):
                                        $closed_cell_feature_list_item = $closed_cell_feature_lists['closed_cell_feature_list_item']; ?>
                                        <li><?php echo $closed_cell_feature_list_item ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>

                            <div class="benefit-link mt-auto p-1">
                                <a class="mf-button" href="<?php echo $closed_cell_download_link['url']; ?>">Download Info Pack</a>
                            </div>

                        </div>
                    </div>
                <?php endif; ?>

            </div>
        </div>
    </section>
<?php endif; ?>