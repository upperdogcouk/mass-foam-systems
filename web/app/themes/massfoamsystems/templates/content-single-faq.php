<?php while (have_posts()) : the_post(); ?>
    <div class="container">
        <div class="page-content-wrap">
            <div class="page-content">
                <div class="row">
                    <div class="col-md">
                        <?php get_template_part('templates/page', 'title'); ?>
                        <?php the_content();?>
                        <?php
                        if (have_rows('people_also_ask')):?>
                            <div class="question-container">
                                <h4>People also ask...</h4>
                                <ul class="my-1">
                                    <?php
                                    while (have_rows('people_also_ask')) : the_row();
                                        $question = get_sub_field('question');
                                        $short_answer = get_sub_field('short_answer');
                                        $link_text = get_sub_field('link_text');
                                        $link = get_sub_field('link');?>
                                        <li>
                                            <div class="question">
                                                <span class="mt-1 mb-0"><?php echo $question;?></span>
                                                <div class="show-more">></div>
                                            </div>
                                            <div class="question-text">
                                                <span><?php echo $short_answer . '..';?></span>
                                                <a href="<?php echo $link;?>" class="mf-button" target="_blank"><?php echo $link_text;?></a>
                                            </div>
                                        </li>
                                        <?php
                                    endwhile;?>
                                </ul>
                            </div>
                            <?php
                        endif;
                        ?>
                        <a href="<?php echo get_post_type_archive_link( 'faq' ); ?>" class="mf-button light-grey-button mt-1">Back to FAQs</a>
                    </div>

                    <?php get_template_part('templates/blocks/faq', 'sidebar'); ?>
                </div>
            </div>
        </div>
    </div>
<?php endwhile; ?>
