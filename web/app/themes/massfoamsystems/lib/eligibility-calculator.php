<?php

// if sessions not started then start it
if (session_status() == PHP_SESSION_NONE)
    session_start();

class EligibilityCalculator
{
    /**
     * Start session and create elibility reference
     */
    function __construct() {
        // Original query args
        $this->start_session();

        // Hook
        $this->hooks();
    }

    /**
     * Start sessions and create submission references
     */
    function start_session() {
        // Generate a order reference in session if not set
        if (empty($_SESSION['form']['reference'])) {
            $this->gen_form_session_reference();
        }
    }

    /**
     * Generate new form session reference
     */
    function gen_form_session_reference($reset = false) {
        // Generate a order reference
        $key = substr(number_format(time() * rand(), 0, '', ''), 0, 6);
        if ($reset) {
            $_SESSION['form']['reference'] = $key;
        } else {
            if (!isset($_SESSION['form']['reference']) || !$_SESSION['form']['reference']) {
                $_SESSION['form']['reference'] = $key;
            }
        }
    }

    /**
     * Function to check if postcode is UK valid
     */
    function is_postcode_valid($postcode) {
        // Remove all whitespaces
        $postcode = preg_replace('/\s/', '', $postcode);

        // Make uppercase
        $postcode = strtoupper($postcode);

        if (preg_match("/^[A-Z]{1,2}[0-9]{2,3}[A-Z]{2}$/", $postcode)
            || preg_match("/^[A-Z]{1,2}[0-9]{1}[A-Z]{1}[0-9]{1}[A-Z]{2}$/", $postcode)
            || preg_match("/^GIR0[A-Z]{2}$/", $postcode)
        ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Send email
     */
    function email_send($to, $subject, $template, $email_vars) {
        // build list of vars for use in email template
        $vars = [];
        array_walk_recursive($email_vars, function($k, $i) use (&$vars) { $vars[str_replace('-','_',$i)] = $k; });
        $vars['template_directory_uri'] = get_template_directory_uri(); // template url

        // adjust array keys
        $nvars = [];
        foreach ($vars as $v_i => $v_k)
            $nvars[str_replace('-','_',$v_i)] = $v_k;
        $vars = $nvars;

        // load email template
        $email_html = file_get_contents(get_template_directory() . '/templates/emails/' . $template . '.html');

        // render the customer email template with variable data
        $email_html = preg_replace_callback("|{{(\w*)}}|", function($m) use($vars) {
            return $vars[$m[1]];
        }, $email_html);

        // send customer email via wordpress's wp_mail
        $headers = array('Content-Type: text/html; charset=UTF-8');
        wp_mail( $to, $subject, $email_html, $headers );
    }

    /**
     * Clear form session
     */
    function clear_form_session() {
        if (isset($_SESSION['form'])) {
            unset($_SESSION['form']);
            $this->gen_form_session_reference();
        }
    }

    /**
     * Progressively save's data on each step
     */
    function progressively_save($step) {
        $existing_sub = $this->submission_exists();

        if ($step == 1 && !$existing_sub) {
            // wp post args
            $wp_post_args = [
                'post_type' => 'eligible',
                'post_title' => '#' . $_SESSION['form']['reference'] . ' eligible submission from ' . $_SESSION['form']['customer_info']['name'],
                'post_status' => 'publish',
                'comment_status' => 'closed',
                'ping_status' => 'closed'
            ];

            // insert the post
            $post_id = wp_insert_post($wp_post_args);

            // set initial post status as 'incomplete'
            update_field('submission_status', 'incomplete', $post_id);
        } else {
            $post_id = $existing_sub;
        }

        // depending on step save data
        switch ($step) {
            case 1:
                // customer details
                update_post_meta( $post_id, 'reference', $_SESSION['form']['reference']);
                update_field( 'client_name', $_SESSION['form']['customer_info']['name'], $post_id );
                update_field( 'client_email', $_SESSION['form']['customer_info']['email'], $post_id );
                update_field( 'client_telephone', $_SESSION['form']['customer_info']['phone'], $post_id );
                update_field( 'client_postcode', $_SESSION['form']['customer_info']['postcode'], $post_id );

                break;
            case 2:
                // insulation area
                update_field('insulation_area', $_SESSION['form']['customer_info']['insulation_area'], $post_id);

                // if the insulation is an area not covered then don't mark it as an incomplete submission
                if ($_SESSION['form']['customer_info']['insulation_area'] == "cavity_wall") {
                    update_field('submission_status', 'invalid', $post_id);
                } else {
                    // make this update as they may change their answers
                    update_field('submission_status', 'incomplete', $post_id);
                }

                break;
            case 3:
                // home owner
                update_field('home_owner', $_SESSION['form']['customer_info']['ownership'], $post_id);

                // if they do not own the home then invalid
                if ($_SESSION['form']['customer_info']['ownership'] == 'no') {
                    update_field('submission_status', 'invalid', $post_id);
                } else {
                    // make this update as they may change their answers
                    update_field('submission_status', 'incomplete', $post_id);
                }

                break;
            case 4:
                // benefits
                update_field('on_benefits', $_SESSION['form']['customer_info']['benefits'], $post_id);

                break;
            case 5:
                // submit (Final)
                update_field('submission_status', 'complete', $post_id);

                break;
            default:
                exit;
        }
    }

    /**
     * Submission exists
     */
    function submission_exists() {
        if (!empty($_SESSION['form']['reference'])) {
            $query = new WP_Query([
                'post_type' => 'eligible',
                'meta_key' => 'reference',
                'meta_value' => $_SESSION['form']['reference']
            ]);
            if ($query->post_count > 0) {
                if (!empty($query->post->ID)) {
                    return $query->post->ID;
                }
            }
        }

        return false;
    }

    /**
     * Ajax Handler
     */
    function ajax() {
        // set feed to be the correct format
        header('Content-type: text/plain');
        ini_set('default_charset', 'utf-8');

        // create empty array to store response
        $response = [];

        // get current step
        $step = (int)$_REQUEST['step'];

        switch ($step) {
            case 1:
                /**
                 * Customer details
                 */

                // create array to store errors
                $errors = array();

                // field validation
                if (!$_REQUEST['name']) $errors['fields']['name'] = 'Please enter your name.';
                if (!preg_match('/^\S+@\S+\.\S+$/', $_REQUEST['email'])) $errors['fields']['email'] = 'Please enter a valid email address.';
                if ((empty($_REQUEST['phone']) || !is_numeric($_REQUEST['phone']))) $errors['fields']['phone'] = 'Please enter a valid phone number.';
                if (!$this->ispostcodevalid($_REQUEST['postcode'])) $errors['fields']['postcode'] = 'Please enter your postcode.';
                if (!isset($_REQUEST['privacy_agreement'])) $errors['fields']['privacy_agreement'] = 'Please confirm the you have read and agree to our Privacy Policy.';

                if (!empty($errors)) {
                    // error response
                    $response['errors'] = $errors;
                } else {
                    // save data
                    $_SESSION['form']['customer_info']['name'] = $_REQUEST['name'];
                    $_SESSION['form']['customer_info']['email'] = $_REQUEST['email'];
                    $_SESSION['form']['customer_info']['phone'] = $_REQUEST['phone'];
                    $_SESSION['form']['customer_info']['postcode'] = $_REQUEST['postcode'];

                    // save current data
                    $this->progressively_save($step);

                    // show next step
                    $response['success'] = [
                        'go_to_step' => 2
                    ];
                }

                break;
            case 2:
                /**
                 * Insulation Area
                 */

                // Check against Mass Foam Systems work area ability (Only pitched roof / loft currently)
                if ($_REQUEST['insulation_area'] == 'other' || $_REQUEST['insulation_area'] == 'cavity_wall') {

                    // If they specify an area which isn't covered by Mass Foam Systems
                    $response['success']['show_elem'] = '#not-applicable-msg';
                    $response['success']['hide_elem'] = '.calc-grant:not(#not-applicable-msg), #eligible-step-3, #eligible-step-4, #eligible-step-5, #submit-step';
                } else {

                    // show next step
                    $response['success'] = [
                        'go_to_step' => 3
                    ];

                    $response['success']['show_elem'] = '#eligible-step-3, #eligible-step-4, #eligible-step-5';
                    $response['success']['hide_elem'] = '.calc-grant';
                }

                // save data to current session
                $_SESSION['form']['customer_info']['insulation_area'] = $_REQUEST['insulation_area'];

                // save current data
                $this->progressively_save($step);

                break;
            case 3:
                /**
                 * Own Property?
                 */

                // Check if the person is applicable and show the relevant message
                if ($_REQUEST['ownership'] == 'no') {

                    // If they aren't applicable
                    $response['success']['show_elem'] = '#not-applicable-msg';
                    $response['success']['hide_elem'] = '.calc-grant:not(#not-applicable-msg), #eligible-step-4, #eligible-step-5, #submit-step';

                } else {

                    // Continue with the form
                    $response['success'] = [
                        'go_to_step' => 4
                    ];

                    $response['success']['show_elem'] = '#eligible-step-4, #eligible-step-5';
                    $response['success']['hide_elem'] = '.calc-grant';

                }

                // save data to session
                $_SESSION['form']['customer_info']['ownership'] = $_REQUEST['ownership'];

                // save information to submission
                $this->progressively_save($step);

                break;
            case 4:
                /**
                 * On Benefits?
                 */

                if ($_REQUEST['benefits'] == 'no') {

                    // If they are applicable for the 5k grant
                    $response['success']['show_elem'] = '#five-k-grant';
                    $response['success']['hide_elem'] = '.calc-grant:not(#five-k-grant)';

                } elseif ($_REQUEST['benefits'] == 'yes') {

                    // If they are applicable for the 10k grant
                    $response['success']['show_elem'] = '#ten-k-grant';
                    $response['success']['hide_elem'] = '.calc-grant:not(#ten-k-grant)';

                }

                // save data to session
                $_SESSION['form']['customer_info']['benefits'] = $_REQUEST['benefits'];

                // save information to submission
                $this->progressively_save($step);


                $response['success']['show_elem'] = $response['success']['show_elem'] . ', #submit-step';

                break;
            case 5:
                /**
                 * Submit Step (Final)
                 */

                // save current data
                $this->progressively_save($step);

                // send email to admin
                $this->email_send('info@massfoamsystems.co.uk', 'New customer submission ref #' . $_SESSION['form']['reference'] . '.', 'submission-client-email', [
                    'clientName'        =>  $_SESSION['form']['customer_info']['name'],
                    'clientPhone'       =>  $_SESSION['form']['customer_info']['phone'],
                    'clientEmail'       =>  $_SESSION['form']['customer_info']['email'],
                    'clientPostcode'    =>  $_SESSION['form']['customer_info']['postcode'],
                    'insulationArea'    =>  str_replace('_', ' ', $_SESSION['form']['customer_info']['insulation_area']),
                    'ownership'         =>  $_SESSION['form']['customer_info']['ownership'],
                    'benefits'          =>  $_SESSION['form']['customer_info']['benefits'],
                    'message'           =>  'Eligible for up to ' . ($_SESSION['form']['customer_info']['benefits'] == 'yes' ? '£10\'000.' : '£5\'000.')
                ]);

                // send email to customer
                $this->email_send($_SESSION['form']['customer_info']['email'], 'Your Mass Foam Systems Eligibility Submission', 'submission-customer-email', [
                    'name'              =>  $_SESSION['form']['customer_info']['name'],
                    'insulationArea'    =>  str_replace('_', ' ', $_SESSION['form']['customer_info']['insulation_area']),
                    'ownership'         =>  $_SESSION['form']['customer_info']['ownership'],
                    'benefits'          =>  $_SESSION['form']['customer_info']['benefits'],
                    'subRef'            =>  $_SESSION['form']['reference'],
                    'nextStepMessage'   =>  'We will be in contact with you shortly.',
                    'grantAmount'       =>  'Eligible for up to ' . ($_SESSION['form']['customer_info']['benefits'] == 'yes' ? '£10\'000.' : '£5\'000.')
                ]);

                // clear the session as it has now been submitted
                $this->clear_form_session();

                $response['success'] = [
                    'go_to_step' => 6
                ];

                $response['success']['hide_elem'] = '.calc-step:not(.submit-success), .calc-grant';
                $response['success']['show_elem'] = '.submit-success';

                break;
            default:
                exit;
        }

        // output json response
        echo json_encode($response);

        wp_die();
    }

    /**
     * Function to check if postcode is UK valid
     */
    function ispostcodevalid($postcode)  {

        //remove all whitespace
        $postcode = preg_replace('/\s/', '', $postcode);

        //make uppercase
        $postcode = strtoupper($postcode);

        if(preg_match("/^[A-Z]{1,2}[0-9]{2,3}[A-Z]{2}$/",$postcode)
            || preg_match("/^[A-Z]{1,2}[0-9]{1}[A-Z]{1}[0-9]{1}[A-Z]{2}$/",$postcode)
            || preg_match("/^GIR0[A-Z]{2}$/",$postcode))
        {
            return true;
        }

    }

    /**
     * Hooks
     */
    private function hooks() {
        // listen for eligiblecalc ajax request
        add_action('wp_ajax_nopriv_eligiblecalc', [$this, 'ajax']);
        add_action('wp_ajax_eligiblecalc', [$this, 'ajax']);

        // set global variable to retrieve ajax url
        add_action('wp_head', [$this, 'set_ajaxurl']);

        // setup admin menu
        add_action('admin_menu', [$this, 'setup_admin_menu']);

        // hook for cron to trigger
        add_action('action_check_hourly_for_unfinished_submissions', [$this, 'check_hourly_for_unfinished_submissions']);
    }

    /**
     * Retrieve unfinished forms in the last hour and email to admin
     */
    function check_hourly_for_unfinished_submissions() {
        // retrieve unfinished submissions made in the last hour.
        $unfinished_subs = new WP_Query([
            'post_type' => 'eligible',
            'meta_key' => 'submission_status',
            'meta_value' => 'incomplete',
            'data_query' => [
                [
                    'before' => '60 minutes ago',
                    'inclusive' => true
                ]
            ],
            'posts_per_page' => -1
        ]);

        if (!empty($unfinished_subs)) {

            // loop through each entry
            foreach ($unfinished_subs->posts as $unfinished_sub) {

                // get submission id
                $id = $unfinished_sub->ID;

                // get submission ref
                $ref = explode(' ', $unfinished_sub->post_title);
                $ref = (isset($ref[0]) ? $ref[0] : '#unknown');

                // get insulation area
                $insulation_area = get_field('insulation_area', $id);

                if ($insulation_area) {
                    $insulation_area = str_replace('_', ' ', $insulation_area);
                }

                // send email to admin
                $this->email_send('info@massfoamsystems.co.uk', 'Incomplete submission request ' . $ref . ' submitted within the last hour.', 'incomplete-submission-client-email', [
                    'clientName'             =>  get_field('client_name', $id),
                    'clientPhone'            =>  get_field('client_telephone', $id),
                    'clientPostcode'         =>  get_field('client_postcode', $id),
                    'clientEmail'            =>  get_field('client_email', $id),
                    'insulationArea'         =>  $insulation_area,
                    'ownProperty'            =>  get_field('home_owner', $id),
                    'onBenefits'             =>  get_field('on_benefits', $id),
                ]);


                $client_email = get_field('client_email', $id);

                if ($client_email) {
                    try {
                        // Try to send reminder to customer
                        $this->email_send($client_email, 'Reminder for incomplete submission request ' . $ref . '.', 'incomplete-submission-customer-reminder', [
                            'name'             =>  get_field('client_name', $id),
                        ]);
                    } catch (Exception $e) {} // Do nothing on error
                }

                // update status to reminded so no more reminders get sent
                update_field('submission_status', 'reminded', $id);
            }
        }
    }

    /**
     * Show submission settings page
     */
    function show_submission_settings_page()  {

        // capture save
        if (isset($_POST['save']) && $_POST['save'] == 'Save') {

            $enable_submission_reminder_emails = (!empty($_POST['enable_submission_reminder_emails']) ? 1 : false );

            // update option field
            update_option( 'enable_submission_reminder_emails' , $enable_submission_reminder_emails );

            // trigger cron setup/removal
            $this->setup_cron($enable_submission_reminder_emails);

        }

        // output admin page html
        echo '<div class="wrap">
                <h1>Submission settings</h1>
                <form method="post">
                    <table class="form-table">
                        <tbody>
                        <tr>
                            <th scope="row">
                                <label for="enable">Enable uncompleted submission reminder emails</label>
                            </th>
                            <td>
                                <input type="checkbox" name="enable_submission_reminder_emails" id="enable_submission_reminder_emails" ' . ( get_option( 'enable_submission_reminder_emails' ) ? ' checked="checked" ' : null )  . ' value="1">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <input name="save" type="submit" class="button button-primary button-large" id="save" value="Save">
                </form>
            </div>';
    }

    /**
     * Set js variable containing ajax url
     */
    function set_ajaxurl() {
        echo '<script type="text/javascript"> window.ajaxurl = "' . admin_url('admin-ajax.php') . '"; </script>' . ("\r\n");
    }

    /**
     * Setup admin menu
     */
    function setup_admin_menu() {
        if (is_admin()) {
            add_submenu_page('edit.php?post_type=eligible', 'Settings', 'Submission Settings', 'manage_options', 'wp-submission-settings', [$this, 'show_submission_settings_page']);
        }
    }

    /**
     * Setup/remove hourly cron
     */
    function setup_cron($setup) {
        if ($setup) {
            if (!wp_next_scheduled('action_check_hourly_for_unfinished_submissions')) {
                wp_schedule_event(time(), 'hourly', 'action_check_hourly_for_unfinished_submissions');
            }
        } else {
            wp_clear_scheduled_hook('action_check_hourly_for_unfinished_submissions');
        }
    }
}

$EligibilityCalculator = new EligibilityCalculator();