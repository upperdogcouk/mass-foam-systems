<?php

// FAQ Category
$labels = array(
    'name' => _x('FAQ Categories', 'taxonomy general name'),
    'singular_name' => _x('FAQ Category', 'taxonomy singular name'),
    'search_items' => __('Search FAQ Categories'),
    'all_items' => __('All FAQ Categories'),
    'parent_item' => __('Parent FAQ Categories'),
    'parent_item_colon' => __('Parent FAQ Category:'),
    'edit_item' => __('Edit FAQ Category'),
    'update_item' => __('Update FAQ Category'),
    'add_new_item' => __('Add New FAQ Category'),
    'new_item_name' => __('New FAQ Category Name'),
    'menu_name' => __('FAQ Categories'),
);

register_taxonomy('faq-categories', array('faq'), array(
    'hierarchical' => TRUE,
    'labels' => $labels,
    'show_ui' => TRUE,
    'show_admin_column' => TRUE,
    'query_var' => TRUE,
    'public' => false

));

// FAQs
$faq_labels = [
    'name' => _x('FAQs', 'Post type general name'),
    'singular_name' => _x('FAQ', 'Post type singular name'),
    'add_new' => _x('Add New', 'FAQ'),
    'add_new_item' => __('Add new FAQ'),
    'edit_item' => __('Edit FAQ'),
    'new_item' => __('New FAQ'),
    'view_item' => __('View FAQ'),
    'view_items' => __('View FAQs'),
    'search_items' => __('Search FAQs'),
    'not_found' => __('No FAQs found'),
    'not_found_in_trash' => __('No FAQs found in Trash'),
    'parent_item_colon' => ''
];

$faq_supports = [
    'title',
    'editor',
    'revisions'
];

register_post_type(
    'faq',
    [
        'labels' => $faq_labels,
        'public' => TRUE,
        'supports' => $faq_supports,
        'has_archive' => TRUE,
        'hierarchical' => TRUE,
        'taxonomies' => array('faq-categories'),
        'menu_icon' => 'dashicons-editor-help',
        'menu_position' => 7,
        'rewrite' => array('slug' => 'insulation-faqs', 'with_front' => FALSE)
    ]
);


// Quotes
$quote_labels = [
    'name' => _x('Quotes', 'Post type general name'),
    'singular_name' => _x('Quote', 'Post type singular name'),
    'add_new' => _x('Add New', 'Quote'),
    'add_new_item' => __('Add new Quote'),
    'edit_item' => __('Edit Quote'),
    'new_item' => __('New Quote'),
    'view_item' => __('View Quote'),
    'view_items' => __('View Quotes'),
    'search_items' => __('Search Quotes'),
    'not_found' => __('No Quotes found'),
    'not_found_in_trash' => __('No Quotes found in Trash'),
    'parent_item_colon' => ''
];

$quote_supports = [
    'title',
    'editor',
    'revisions'
];

register_post_type(
    'quote',
    [
        'labels' => $quote_labels,
        'public' => true,
        'publicly_queryable' => false,
        'supports' => $quote_supports,
        'has_archive' => false,
        'hierarchical' => false,
        'menu_icon' => 'dashicons-feedback',
        'menu_position' => 8,
    ]
);

// Eligibility Submissions
$eligible_labels = [
    'name' => _x('Eligible', 'Post type general name'),
    'singular_name' => _x('Submission', 'Post type singular name'),
    'add_new' => _x('Add New', 'Submission'),
    'add_new_item' => __('Add new Submission'),
    'edit_item' => __('Edit Submission'),
    'new_item' => __('New Submission'),
    'view_item' => __('View Submission'),
    'view_items' => __('View Submission'),
    'search_items' => __('Search Submission'),
    'not_found' => __('No Submission found'),
    'not_found_in_trash' => __('No Submission found in Trash'),
    'parent_item_colon' => ''
];

$eligible_supports = [
    'title',
    'editor',
    'revisions'
];

register_post_type('eligible', [
    'labels' => $eligible_labels,
    'public' => true,
    'publicly_queryable' => false,
    'supports' => $eligible_supports,
    'has_archive' => false,
    'hierarchical' => false,
    'menu_icon' => 'dashicons-yes-alt',
    'menu_position' => 8,
]);

// Eco Calc Submissions
$eco_labels = [
    'name' => _x('ECO Submissions', 'Post type general name'),
    'singular_name' => _x('ECO', 'Post type singular name'),
    'add_new' => _x('Add New', 'Submission'),
    'add_new_item' => __('Add new Submission'),
    'edit_item' => __('Edit Submission'),
    'new_item' => __('New Submission'),
    'view_item' => __('View Submission'),
    'view_items' => __('View Submission'),
    'search_items' => __('Search Submission'),
    'not_found' => __('No Submission found'),
    'not_found_in_trash' => __('No Submission found in Trash'),
    'parent_item_colon' => ''
];

$eco_supports = [
    'title',
    'editor',
    'revisions'
];

register_post_type('eco-calc', [
    'labels' => $eco_labels,
    'public' => true,
    'publicly_queryable' => false,
    'supports' => $eco_supports,
    'has_archive' => false,
    'hierarchical' => false,
    'menu_icon' => 'dashicons-lightbulb',
    'menu_position' => 8,
]);
