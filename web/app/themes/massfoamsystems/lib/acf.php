<?php

/**
 * Add options pages using ACF (advanced custom fields) for
 * globally accessed information on the site (used in things
 * like headers and footers, etc).
 */

if (function_exists("acf_add_options_page")) {

  // Add Theme Options page
  acf_add_options_sub_page([
    'title' => 'Site Options',
    'menu_slug' => 'site_options',
    'parent' => 'options-general.php',
    'capability' => 'edit_posts'
  ]);
}

/**
 * Show Order & Location columns in ACF Field Groups Admin Page
 */

function acf_field_group_columns($columns) {
  $columns['menu_order'] = __('Order');
  $columns['location'] = __('Location Rules');
  return $columns;
} // end function reference_columns
add_filter('manage_edit-acf-field-group_columns', 'acf_field_group_columns', 20);

function acf_field_group_columns_content($column, $post_id) {
  $post = get_post($post_id);
  switch ($column) {
    case 'menu_order':
      global $post;
      echo '<strong>',$post->menu_order,'</strong>';
      break;
    case 'location':
      $details = unserialize($post->post_content);
      $location = $details['location'];
      if (is_array($location) && count($location)) {
        echo '<pre>';
        $or_count = 1;
        foreach ($location as $or) {
          $and_count = 1;
          foreach ($or as $and) {
            if ($and_count == 1) {
              echo '    ';
            }
            echo $and['param'],' ',$and['operator'],' ',$and['value'];
            if ($and_count < count($or)) {
              echo "\r\n",'<strong>AND </strong>';
            }
            $and_count++;
          }
          if ($or_count < count($location)) {
            echo "\r\n\r\n",'<strong>OR</strong>',"\r\n";
          }
          $or_count++;
        }
        echo '</pre>';
      }
      break;
    default:
      break;
  } // end switch
} // end function reference_columns_content
add_action('manage_acf-field-group_posts_custom_column', 'acf_field_group_columns_content', 20, 2);


/**
 * Parse the video uri/url to determine the video type/source and the video id
 */
function parse_video_uri( $url ) {

  // Parse the url
  $parse = parse_url( $url );

  // Set blank variables
  $video_type = '';
  $video_id = '';

  // Url is http://youtu.be/xxxx
  if ( $parse['host'] == 'youtu.be' ) {
    $video_type = 'youtube';
    $video_id = ltrim( $parse['path'],'/' );

  }

  // Url is http://www.youtube.com/watch?v=xxxx
  // or http://www.youtube.com/watch?feature=player_embedded&v=xxx
  // or http://www.youtube.com/embed/xxxx
  if ( ( $parse['host'] == 'youtube.com' ) || ( $parse['host'] == 'www.youtube.com' ) ) {

    $video_type = 'youtube';

    parse_str( $parse['query'], $output );

    $video_id = $output['v'];

    if ( !empty( $feature ) )
      $video_id = end( explode( 'v=', $parse['query'] ) );

    if ( strpos( $parse['path'], 'embed' ) == 1 )
      $video_id = end( explode( '/', $parse['path'] ) );

  }


  $host_names = explode(".", $parse['host'] );
  $rebuild = ( ! empty( $host_names[1] ) ? $host_names[1] : '') . '.' . ( ! empty($host_names[2] ) ? $host_names[2] : '');

  // If recognised type return video array
  if ( !empty( $video_type ) ) {

    $video_array = array(
      'type' => $video_type,
      'id' => $video_id
    );

    return $video_array;

  } else {
    return false;
  }
}