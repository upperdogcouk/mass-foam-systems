<?php

// if session not started then start it
if (session_status() == PHP_SESSION_NONE)
    session_start();

class QuoteCalculator
{


    /**
     * Start session and create quote reference
     */
    function __construct()  {

        // original query args
        $this->start_session();

        // hooks
        $this->hooks();

    }


    /**
     * Hooks
     */
    private function hooks()  {

        // listen for quotecalc ajax request
        add_action( 'wp_ajax_nopriv_quotecalc', [$this, 'ajax'] );
        add_action( 'wp_ajax_quotecalc', [$this, 'ajax'] );

        // set global variable to retrieve ajax url
        add_action('wp_head', [$this, 'set_ajaxurl']);

        // setup admin menu
        add_action('admin_menu', [$this, 'setup_admin_menu']);

        // hook for cron to trigger
        add_action('action_check_hourly_for_unfinished_quotations', [&$this, 'check_hourly_for_unfinished_quotations']);

    }


    /**
     * Setup admin menu
     */
    function setup_admin_menu()  {

        if ( is_admin() )
            add_submenu_page('edit.php?post_type=quote', 'Settings', 'Quote Settings', 'manage_options', 'wp-quote-settings', [$this, 'show_quote_settings_page'] );

    }


    /**
     * Setup/remove hourly cron
     */
    function setup_cron($setup)  {

        if ($setup) {
            if (!wp_next_scheduled('action_check_hourly_for_unfinished_quotations')) {
                wp_schedule_event(time(), 'hourly', 'action_check_hourly_for_unfinished_quotations' );
            }
        } else
            wp_clear_scheduled_hook( 'action_check_hourly_for_unfinished_quotations' );

    }


    /**
     * Retrieve unfinished quotes made in the last hour and email admin to inform customer may need help
     */
    function check_hourly_for_unfinished_quotations()  {

        // retrieve unfinished quotes made in the last hour.
        $unfinished_quotes = new WP_Query( ['post_type' => 'quote','meta_key' => 'quote_status', 'meta_value' => 'incomplete', 'date_query' => [['before' => '60 minutes ago', 'inclusive' => true]], 'posts_per_page' => -1,] );

        // if unfinished quotes found over an hour old
        if (!empty($unfinished_quotes->posts)):

            foreach ($unfinished_quotes->posts as $unfinished_quote){

                // get quote id
                $id = $unfinished_quote->ID;

                // get quote ref
                $ref = explode(" ", $unfinished_quote->post_title);
                $ref = (isset($ref[0]) ? $ref[0] : '#unkown');

                // send email to admin
                $this->email_send('info@massfoamsystems.co.uk', 'Incomplete quotation request ' . $ref  . ' submitted an hour ago.', 'incomplete-quote-client-email', [
                    'clientName'             =>  get_field('client_name', $id),
                    'clientPhone'            =>  get_field('client_telephone', $id),
                    'clientEmail'            =>  get_field('client_email', $id),
                    'clientPostcode'         =>  get_field('client_postcode', $id),
                    'propertyType'           =>  get_field('property_sector', $id),
                    'propertySize'           =>  get_field('property_size', $id),
                    'foamThickness'          =>  get_field('foam_thickness', $id),
                    'quotePrice'             =>  get_field('quoted_price', $id),
                ]);

                // update status to reminded so no more reminders get sent
                update_field( "quote_status", 'reminded', $id );

            }

        endif;

    }


    /**
     * Show quote settings page
     */
    function show_quote_settings_page()  {

        // capture save
        if (isset($_POST['save']) && $_POST['save'] == 'Save') {

            $enable_quote_reminder_emails = (!empty($_POST['enable_quote_reminder_emails']) ? 1 : false );

            // update option field
            update_option( 'enable_quote_reminder_emails' , $enable_quote_reminder_emails );

            // trigger cron setup/removal
            $this->setup_cron($enable_quote_reminder_emails);

        }

        // output admin page html
        echo '<div class="wrap">
                <h1>Quotation settings</h1>
                <form method="post">
                    <table class="form-table">
                        <tbody>
                        <tr>
                            <th scope="row">
                                <label for="enable">Enable uncompleted quote reminder emails</label>
                            </th>
                            <td>
                                <input type="checkbox" name="enable_quote_reminder_emails" id="enable_quote_reminder_emails" ' . ( get_option( 'enable_quote_reminder_emails' ) ? ' checked="checked" ' : null )  . ' value="1">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <input name="save" type="submit" class="button button-primary button-large" id="save" value="Save">
                </form>
            </div>';

    }


    /**
     * Set js variable containing ajax url
     */
    function set_ajaxurl()  {

        echo '<script type="text/javascript"> window.ajaxurl = "' . admin_url('admin-ajax.php') . '"; ' . '</script>' . ("\r\n");

    }


    /**
     * Price bands lookup function
     */
    function pricing_bands($property_size, $thickness)  {
       $thickness = '100mm';

       $bands = [
           '100mm'  =>  ['50-70sqm' => 25, '70-90sqm' => 23, '90+sqm' => 20],
           '150mm'  =>  ['50-70sqm' => 30, '70-90sqm' => 28, '90+sqm' => 25],
           '200mm'  =>  ['50-70sqm' => 35, '70-90sqm' => 32, '90+sqm' => 30],
       ];

       return (isset($bands[$thickness][$property_size]) ? $bands[$thickness][$property_size] : false);

    }


    /**
     * Calculate price returning a between price
     */
    function calc_price($property_size, $thickness)  {

        // get per square meter price
        $psqm_price = $this->pricing_bands($property_size, $thickness);

        // calculate price based on property size
        if (strpos($property_size, '-') !== false) {

            // between price
            $exploded_sizes = explode('-', $property_size );
            $low = (int)$exploded_sizes[0];
            $high = (int)$exploded_sizes[1];
            return '&pound;' . ($psqm_price*$low) . ' - &pound;' . ($psqm_price*$high);

        } elseif (strpos($property_size, '+') !== false) {

            // from price
            $low = (int)$property_size;
            return 'From &pound;' . ($psqm_price*$low);

        }

    }


    /**
     * Progressively save's data on each step
     */
    function progressively_save($step)  {

        // check if quote exists based on session ref
        $existing_quote = $this->quote_exists();

        if ($step == 1 && !$existing_quote){

            // wp post args
            $wp_post_args = [
                'post_type' => 'quote',
                'post_title' => '#' .  $_SESSION["quote"]["reference"] . " quotation request from " . $_SESSION["quote"]["customer_info"]["name"],
                'post_status' => 'publish',
                'comment_status' => 'closed',
                'ping_status' => 'closed'
            ];

            // insert the post
            $post_id = wp_insert_post($wp_post_args);

            // set initial post status as `incomplete`
            update_field( "quote_status", 'incomplete ', $post_id );

        } else {

            $post_id = $existing_quote;

        }

        // depending on step save data
        switch ($step) {
            case 1:

                // customer details
                update_post_meta( $post_id,'reference', $_SESSION["quote"]["reference"]);
                update_field( "client_name", $_SESSION["quote"]["customer_info"]["name"], $post_id );
                update_field( "client_email", $_SESSION["quote"]["customer_info"]["email"], $post_id );
                update_field( "client_telephone", $_SESSION["quote"]["customer_info"]["phone"], $post_id );
                update_field( "client_postcode", $_SESSION["quote"]["customer_info"]["postcode"], $post_id );

                break;
            case 2:

                // property sector
                update_field( "property_sector", $_SESSION["quote"]["sector"], $post_id );

                break;
            case 3:

                // how big is your property
                update_field( "property_size", $_SESSION["quote"]["size-of-property"], $post_id );

                break;
            case 4:

                // thickness of spray foam (final save)
                update_field( "foam_thickness", $_SESSION["quote"]["thickness-of-spray_foam"], $post_id );
                update_field( "quoted_price", $_SESSION["quote"]["quoted-price"], $post_id );
                update_field( "quote_status", 'complete', $post_id );

                break;
            default:
                exit;
        }

    }


    /**
     * Quote exists
     */
    function quote_exists()  {
        if (!empty($_SESSION["quote"]["reference"])):
            $query = new WP_Query([
                'post_type' => 'quote',
                'meta_key' => 'reference',
                'meta_value' => $_SESSION["quote"]["reference"]
            ]);
            if ($query->post_count > 0){
                if (!empty($query->post->ID))
                    return $query->post->ID;
            }

        endif;
    }


    /**
     * Function to check if postcode is UK valid
     */
    function ispostcodevalid($postcode)  {

        //remove all whitespace
        $postcode = preg_replace('/\s/', '', $postcode);

        //make uppercase
        $postcode = strtoupper($postcode);

        if(preg_match("/^[A-Z]{1,2}[0-9]{2,3}[A-Z]{2}$/",$postcode)
            || preg_match("/^[A-Z]{1,2}[0-9]{1}[A-Z]{1}[0-9]{1}[A-Z]{2}$/",$postcode)
            || preg_match("/^GIR0[A-Z]{2}$/",$postcode))
        {
            return true;
        }

    }


    /**
     * Send email
     */
    function email_send($to, $subject, $template, $email_vars)  {

        // build list of vars for use in email template
        $vars = [];
        array_walk_recursive($email_vars, function($k, $i) use (&$vars) { $vars[str_replace("-","_",$i)] = $k; });
        $vars['template_directory_uri'] = get_template_directory_uri(); // template url

        // adjust array keys
        $nvars = [];
        foreach ($vars as $v_i => $v_k)
            $nvars[str_replace("-","_",$v_i)] = $v_k;
        $vars = $nvars;

        // load email template
        $email_html = file_get_contents(get_template_directory() . '/templates/emails/' . $template . '.html');

        // render the customer email template with variable data
        $email_html = preg_replace_callback("|{{(\w*)}}|", function($m) use($vars) {
            return $vars[$m[1]];
        }, $email_html);

        // send customer email via wordpress's wp_mail
        $headers = array('Content-Type: text/html; charset=UTF-8');
        wp_mail( $to, $subject, $email_html, $headers );

    }


    /**
     * Ajax Global Handler
     */
    function ajax_global()  {

        // set feed to be the correct format
        header('Content-type: text/plain');
        ini_set('default_charset', 'utf-8');

        // request call back
        if (isset($_REQUEST["request_a_callback"]) && $_REQUEST["request_a_callback"]) {

            // send admin email informing of callback request
            $this->email_send('info@massfoamsystems.co.uk', 'Customer requested a call back ref #' . $_SESSION["quote"]["reference"]  . '.', 'callback-client-email', [
                'clientName'             =>  $_SESSION["quote"]["customer_info"]["name"],
                'clientPhone'            =>  $_SESSION["quote"]["customer_info"]["phone"],
                'clientEmail'            =>  $_SESSION["quote"]["customer_info"]["email"],
                'propertySector'         =>  $_SESSION["quote"]["sector"],
            ]);

            // mark as complete
            update_field( "quote_status", 'complete', $this->quote_exists() );

            // clear quote session as its now been submitted
            $this->clear_quote_session();

            // ajax response
            echo json_encode([
                'success'   => [
                    'go_to_step' => 'message',
                    'message_id' => 'callback_success',
                ]
            ]);
            wp_die();

        }

    }


    /**
     * Ajax Handler
     */
    function ajax()  {


        // set feed to be the correct format
        header('Content-type: text/plain');
        ini_set('default_charset', 'utf-8');

        // start empty array to store response
        $response = [];

        // get current step
        $step = (int)$_REQUEST["step"];

        switch ($step) {
            case 1:

                /**
                 * Customer details
                 */

                // create array to store errors
                $errors = array();

                // field validation
                if (!$_REQUEST["name"]) $errors['fields']['name'] = 'Please enter your name.';
                if (!preg_match('/^\S+@\S+\.\S+$/', $_REQUEST["email"])) $errors['fields']['email'] = 'Please enter a valid email address.';
                if ((empty($_REQUEST["phone"]) || !is_numeric($_REQUEST["phone"]))) $errors['fields']['phone'] = 'Please enter a valid phone number.';
                if (!$this->ispostcodevalid($_REQUEST["postcode"])) $errors['fields']['postcode'] = 'Please enter your postcode.';
                if (!isset($_REQUEST["privacy_agreement"])) $errors['fields']['privacy_agreement'] = 'Please confirm the you have read and agree to our Privacy Policy.';

                // if errors
                if(!empty($errors)) {

                    // error response
                    $response['errors'] = $errors;

                } else {

                    // save data
                    $_SESSION["quote"]["customer_info"]["name"] = $_REQUEST["name"];
                    $_SESSION["quote"]["customer_info"]["email"] = $_REQUEST["email"];
                    $_SESSION["quote"]["customer_info"]["phone"] = $_REQUEST["phone"];
                    $_SESSION["quote"]["customer_info"]["postcode"] = $_REQUEST["postcode"];

                    // save current data
                    $this->progressively_save($step);

                    // show next step
                    $response['success'] = [
                        'go_to_step' => 2
                    ];

                }

                break;
            case 2:

                /**
                 * Sector
                 */

                // field validation
                if (empty($_REQUEST["sector"])) $errors['fields']['sector'] = 'Please select a sector.';


                // if errors
                if(!empty($errors)) {

                    // error repose
                    $response['errors'] = $errors;

                } else {

                    // save data
                    $_SESSION["quote"]["sector"] = $_REQUEST["sector"];

                    // save current data
                    $this->progressively_save($step);

                    // show next step
                    $response['success'] = [
                        'go_to_step' => 3
                    ];

                    // show hide messages
                    // if ($_REQUEST["sector"] == 'Residential') {
                    //     $response['success']['show_elem'] = '.3-residential-message';
                    //     $response['success']['hide_elem'] = '.3-other-message';
                    // } else {
                    //     $response['success']['show_elem'] = '.3-other-message';
                    //     $response['success']['hide_elem'] = '.3-residential-message,#quote-step-4,#quote-step-5';
                    // }

                    // Disable quotation
                    $response['success']['show_elem'] = '.3-other-message';
                    $response['success']['hide_elem'] = '.3-residential-message,#quote-step-4,#quote-step-5';

                }

                break;
            case 3:

                /**
                 * Size of property
                 */

                // field validation
                if ( empty($_REQUEST["size_of_property"]) ) $errors['fields']['size_of_property'] = 'Please tell us the size of your property.';

                // if errors
                if(!empty($errors)) {

                    // error response
                    $response['errors'] = $errors;

                } else {

                    // save data to session
                    $_SESSION["quote"]["size-of-property"] = $_REQUEST["size_of_property"];

                    // save current data
                    $this->progressively_save($step);

                    // send emails


                    // go to next step
                    $response['success'] = [
                        'go_to_step' => 4
                    ];

                }

                break;
            case 4:

                /**
                 * Thickness of spray foam (Final)
                 */

                // field validation
                if ( empty($_REQUEST["thickness_of_spray_foam"]) ) $errors['fields']['thickness_of_spray_foam'] = 'Please tell us the thickness of spray foam you require.';

                // if errors
                if(!empty($errors)) {

                    // error response
                    $response['errors'] = $errors;

                } else {

                    // save data to session
                    $_SESSION["quote"]["thickness-of-spray_foam"] = $_REQUEST["thickness_of_spray_foam"];

                    // store calculated price if both size property's set
                    if (empty($_SESSION["quote"]["size-of-property"]) || empty($_SESSION["quote"]["thickness-of-spray_foam"]))
                        $_SESSION["quote"]["quoted-price"] = null;
                    else
                        $_SESSION["quote"]["quoted-price"] = $this->calc_price($_SESSION["quote"]["size-of-property"], $_SESSION["quote"]["thickness-of-spray_foam"]); // calculate

                    // save current data
                    $this->progressively_save($step);

                    // send email to admin
                    $this->email_send('info@massfoamsystems.co.uk', 'New customer quote created ref #' . $_SESSION["quote"]["reference"]  . '.', 'quote-client-email', [
                        'clientName'             =>  $_SESSION["quote"]["customer_info"]["name"],
                        'clientPhone'            =>  $_SESSION["quote"]["customer_info"]["phone"],
                        'clientEmail'            =>  $_SESSION["quote"]["customer_info"]["email"],
                        'clientPostcode'         =>  $_SESSION["quote"]["customer_info"]["postcode"],
                        'propertyType'           =>  $_SESSION["quote"]["sector"],
                        'propertySize'           =>  $_SESSION["quote"]["size-of-property"],
                        'foamThickness'          =>  $_SESSION["quote"]["thickness-of-spray_foam"],
                        'quotePrice'             =>  $_SESSION["quote"]["quoted-price"],
                    ]);

                    // send email to customer with overview of quotation request if we were able to calculate price
                    if ($_SESSION["quote"]["quoted-price"]):
                        $this->email_send($_SESSION["quote"]["customer_info"]["email"], 'Your Mass Foam Systems Instant Quote.', 'quote-customer-email', [
                            'firstName'              =>  $_SESSION["quote"]["customer_info"]["name"],
                            'propertySize'           =>  $_SESSION["quote"]["size-of-property"],
                            'foamThickness'          =>  $_SESSION["quote"]["thickness-of-spray_foam"],
                            'quotePrice'             =>  $_SESSION["quote"]["quoted-price"],
                            'quoteRef'               =>  '#' . $_SESSION["quote"]["reference"],
                            'nextStepMessage'        =>  (($_SESSION["quote"]["quoted-price"] == '&pound;0 - &pound;0' || $_SESSION["quote"]["quoted-price"] == 'From &pound;0' ) ? 'As you have stated "Don\'t know" for one or more questions, we\'ll need to get in touch with you to help get you an accurate right quote. We will be in touch soon - or call us on 0800 246 5051 to speak to one of our contractors directly' : 'One of our friendly experts will give you a call soon! Or call us on 0800 246 5051 to speak to one of our contractors directly'),

                        ]);
                    endif;

                    // clear quote session as its now been submitted
                    $this->clear_quote_session();

                    // go to next step
                    $response['success'] = [
                        'go_to_step' => 5
                    ];

                    // show hide elements
                    $response['success']['show_elem'] = '.submit-success';
                    $response['success']['hide_elem'] = '.calc-step:not(.submit-success)';

                }

                break;
            default:
                exit;
        }

        // global ajax handler - override's json output depending on request.
        $this->ajax_global();

        // output json response
        echo json_encode($response);

        wp_die();

    }


    /**
     * Start session and create quote reference
     */
    function start_session()
    {

        // generate a order reference in session if not set
        if (empty($_SESSION["quote"]["reference"])){
            $this->gen_quote_session_reference();
        }

    }


    /**
     * Clear quote session
     */
    function clear_quote_session()
    {
        if(isset($_SESSION["quote"])){
            unset($_SESSION["quote"]);
            $this->gen_quote_session_reference();
        }
    }


    /**
     * Generate new quote session reference
     */
    function gen_quote_session_reference($reset = false)
    {

        // generate a order reference
        $key = substr(number_format(time() * rand(),0,'',''),0,6);
        if ($reset)
            $_SESSION["quote"]["reference"] = $key;
        else {
            if (!isset($_SESSION["quote"]["reference"]) || !$_SESSION["quote"]["reference"])
                $_SESSION["quote"]["reference"] = $key;
        }

    }


}

$QuoteCalculator = new QuoteCalculator();