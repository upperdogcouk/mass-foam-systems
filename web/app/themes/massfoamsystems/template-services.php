<?php
/**
 * Template Name: Services
 */

while (have_posts()) : the_post();
    get_template_part('templates/blocks/global', 'page-hero');
    get_template_part('templates/content', 'services');
    get_template_part('templates/blocks/global', 'why-use-mass-foam');
    get_template_part('templates/blocks/global', 'accredited-trustworthy');
    get_template_part('templates/blocks/global', 'footer-contact-cta');

endwhile;